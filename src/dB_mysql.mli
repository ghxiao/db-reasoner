val mysql_error2db_error : Mysql.error_code -> DB_sig.error_code
val raise_error : Mysql.dbd -> unit
val mysql_dbty2db_dbty : Mysql.dbty -> DB_sig.dbty
module S :
  sig
    type db_handle = Mysql.dbd
    val connect :
      string option ->
      int option ->
      string option -> string option -> string option -> db_handle
    type query_result = Mysql.result
    val query : Mysql.dbd -> string -> query_result
    val fetch_array : Mysql.result -> string option array option
    val num_rows : Mysql.result -> int64
    val insert_id : Mysql.dbd -> int64
    val escape : string -> string
  end
module DBmysql :
  sig
    type db_handle = S.db_handle
    val connect :
      string option ->
      int option ->
      string option -> string option -> string option -> S.db_handle
    type query_result = S.query_result
    val query : S.db_handle -> string -> S.query_result
    val query_list : S.db_handle -> bool -> string list -> unit    
    val fetch_array : S.query_result -> string option array option
    val num_rows : S.query_result -> int64
    val insert_id : S.db_handle -> int64
    val escape : string -> string
    val table_exist : S.db_handle -> string -> bool
    val drop_table : S.db_handle -> string -> unit
    val fetch_int : S.query_result -> int
    val row_count : S.db_handle -> int
    val table_count : S.db_handle -> string -> int
  end
