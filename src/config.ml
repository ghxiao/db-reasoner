
(** Permanent table ontology informations **)
let tbl_info        = "info"
let tbl_info_create = "(type ENUM('ontologyIRI','filename','load','classify','taxonomy','output') NOT NULL,\
                        value_text TEXT NOT NULL,\
                        value_int INT NOT NULL,\
                        PRIMARY KEY (type))"

(** Permanent tables for loading **)
let tbl_id_object_atm        = "id_object_atm"
let tbl_id_object_atm_create = Printf.sprintf "CREATE TABLE %s (id INT NOT NULL,\
                                                                name VARCHAR(10) NOT NULL,\
                                                                PRIMARY KEY (name),\
                                                                INDEX (id)\
                                                               ) PARTITION BY KEY() PARTITIONS 10" tbl_id_object_atm
                                                           
let tbl_id_class_atm        = "id_class_atm"
let tbl_id_class_atm_create = Printf.sprintf "CREATE TABLE %s (id INT NOT NULL,\
                                                               name VARCHAR(10) NOT NULL,\
                                                               PRIMARY KEY (name),\
                                                               INDEX (id)\
                                                              ) PARTITION BY KEY() PARTITIONS 10" tbl_id_class_atm
                                                        
let tbl_id_class_int        = "id_class_int"
let tbl_id_class_int_create = Printf.sprintf "CREATE TABLE %s (id INT NOT NULL,\
                                                               id1 INT NOT NULL,\
                                                               id2 INT NOT NULL,\
                                                               positive BOOL NOT NULL,\
                                                               negative BOOL NOT NULL,\
                                                               update_id INT,\
                                                               INDEX (update_id),\                                                               
                                                               PRIMARY KEY (id1,id2),\
                                                               INDEX (id2)\
                                                              )" tbl_id_class_int

let tbl_id_class_ext        = "id_class_ext"
let tbl_id_class_ext_create = Printf.sprintf "CREATE TABLE %s (id INT NOT NULL,\
                                                               id1 INT NOT NULL,\
                                                               id2 INT NOT NULL,\            
                                                               positive BOOL NOT NULL,\
                                                               negative BOOL NOT NULL,\
                                                               update_id INT,\
                                                               INDEX (update_id),\
                                                               PRIMARY KEY (id1,id2)\
                                                              )" tbl_id_class_ext

let tbl_ax_class        = "ax_class"
let tbl_ax_class_create = Printf.sprintf "CREATE TABLE %s (sub INT NOT NULL,\
                                                           sup INT NOT NULL,\
                                                           PRIMARY KEY (sub,sup)\
                                                           )" tbl_ax_class

let tbl_ax_class_compl        = "ax_class_compl"
let tbl_ax_class_compl_create = Printf.sprintf "CREATE TABLE %s (sub INT NOT NULL,\
                                                                 sup INT NOT NULL,\
                                                                 PRIMARY KEY (sub,sup)\
                                                                )" tbl_ax_class_compl                                                           
                                                             
let tbl_ax_object        = "ax_object"
let tbl_ax_object_create = Printf.sprintf "CREATE TABLE %s (sub INT NOT NULL,\
                                                            sup INT NOT NULL,\
                                                            PRIMARY KEY (sub,sup)\
                                                           )" tbl_ax_object  

(** Permanent tables for classification **)
                                                                              
let tbl_cl_object        = "cl_object"
let tbl_cl_object_create = Printf.sprintf "CREATE TABLE %s (sub INT NOT NULL,\
                                                            sup INT NOT NULL\
                                                           )" tbl_cl_object
                           
let tbl_cl_class        = "cl_class"
let tbl_cl_class_create = Printf.sprintf "CREATE TABLE %s (sub INT NOT NULL,\
                                                           sup INT NOT NULL,\
                                                           new BOOL NOT NULL,\
                                                           INDEX (sub,sup),\
                                                           INDEX (sup)\
                                                          )" tbl_cl_class

(** Permanent tables for taxonomy **)
let tbl_tx_imply_o        = "tx_impl_o"
let tbl_tx_imply_o_create = Printf.sprintf "CREATE TABLE %s (sub INT NOT NULL,\
                                                             sup INT NOT NULL,\
                                                             INDEX (sup),\                                                             
                                                             PRIMARY KEY (sub,sup)\
                                                            )" tbl_tx_imply_o
let tbl_tx_equiv_o        = "tx_equi_o"
let tbl_tx_equiv_o_create = Printf.sprintf "CREATE TABLE %s (repr INT NOT NULL,\
                                                             id INT NOT NULL,\
                                                             INDEX (repr),\
                                                             PRIMARY KEY (id)\
                                                            )" tbl_tx_equiv_o
                                                           
let tbl_tx_imply_c        = "tx_impl_c"
let tbl_tx_imply_c_create = Printf.sprintf "CREATE TABLE %s (sub INT NOT NULL,\
                                                             sup INT NOT NULL,\
                                                             INDEX (sup),\
                                                             PRIMARY KEY (sub,sup)\
                                                            )" tbl_tx_imply_c
let tbl_tx_equiv_c        = "tx_equi_c"
let tbl_tx_equiv_c_create = Printf.sprintf "CREATE TABLE %s (repr INT NOT NULL,\
                                                             id INT NOT NULL,\
                                                             INDEX (repr),\
                                                             PRIMARY KEY (id)\
                                                            )" tbl_tx_equiv_c                                  
                                                           
