open DB_mysql
module DB = DBmysql

(** Drops output tables **) 
let drop_tmp_tables db =
  DB.drop_table db Config_output.tbl_tmp_imply;
  DB.drop_table db Config_output.tbl_tmp_out_imply;
  DB.drop_table db Config_output.tbl_tmp_equiv;
  DB.drop_table db Config_output.tbl_tmp_repr;
  DB.drop_table db Config_output.tbl_tmp_out_equiv
  
(** Creates output tables **) 
let create_tables db =
  DB.query_list db false [
    Config_output.tbl_tmp_imply_create;
    Config_output.tbl_tmp_out_imply_create;
    Config_output.tbl_tmp_equiv_create;
    Config_output.tbl_tmp_repr_create;
    Config_output.tbl_tmp_out_equiv_create]

(** Outputs classification on standard output **)  
let output file =
  let db = Common.open_connection () in  
  (** Check for status **)
  let (s,_) = Common.get_status db "taxonomy" in
  if (String.compare s "done") != 0 && (String.compare s "deprecated") != 0 then
    (Printf.fprintf stderr "The taxonomy has not been computed. Use './db -d database_name -t' to compute it.\n"; flush stderr)
  else begin

  if (String.compare s "deprecated") = 0 then
    (Printf.fprintf stderr "WARNING: Ontology's taxonomy is deprecated. Use './db -d database_name -t' to compute it again.\n"; flush stderr);
  
  Printf.fprintf stderr "Outputing taxonomy:\n"; flush stderr; 
  drop_tmp_tables db;
  create_tables db;

  let max_len = DB.fetch_int (DB.query db (Printf.sprintf "SELECT MAX(LENGTH(name)) FROM %s" Config.tbl_id_class_atm)) in
  List.iter (fun (table,id) -> let _ = DB.query db (Printf.sprintf "ALTER TABLE %s CHANGE %s %s VARCHAR(%d)" table id id max_len) in ())
            [(Config_output.tbl_tmp_out_imply,"sub");(Config_output.tbl_tmp_out_imply,"sup");
             (Config_output.tbl_tmp_out_equiv,"sub");(Config_output.tbl_tmp_out_equiv,"sup");
             (Config_output.tbl_tmp_imply,"sub"); (Config_output.tbl_tmp_equiv,"id"); (Config_output.tbl_tmp_repr,"name")];

  let begt = Unix.gettimeofday () in
  Printf.fprintf stderr "1. Resolving equiv's name... "; flush stderr;   

  DB.query_list db true [
     Printf.sprintf "INSERT INTO %s SELECT t1.repr,t2.name FROM %s AS t1 JOIN %s AS t2 ON t1.id=t2.id"
                     Config_output.tbl_tmp_equiv Config.tbl_tx_equiv_c Config.tbl_id_class_atm;
     Printf.sprintf "INSERT INTO %s SELECT repr,MIN(id) FROM %s GROUP BY repr"
                     Config_output.tbl_tmp_repr Config_output.tbl_tmp_equiv;
     Printf.sprintf "INSERT IGNORE INTO %s SELECT t1.name,t2.id FROM %s AS t1 JOIN %s AS t2 ON t1.repr=t2.repr AND t1.name!=t2.id"
                     Config_output.tbl_tmp_out_equiv Config_output.tbl_tmp_repr Config_output.tbl_tmp_equiv];
  let endt = Unix.gettimeofday () in Printf.fprintf stderr " done in %fs\n" (endt -. begt); flush stderr;
  
  let begt = Unix.gettimeofday () in
  Printf.fprintf stderr "2. Resolving imply's name... "; flush stderr;   
  DB.query_list db true [
     Printf.sprintf "INSERT IGNORE INTO %s SELECT t2.name,t1.sub,t1.sup FROM %s AS t1 JOIN %s AS t2 ON t1.sub=t2.repr" 
                     Config_output.tbl_tmp_imply Config.tbl_tx_imply_c Config_output.tbl_tmp_repr; 
     Printf.sprintf "INSERT IGNORE INTO %s SELECT t2.name,t1.sub,t1.sup FROM %s AS t1 JOIN %s AS t2 ON t1.sub=t2.id" 
                     Config_output.tbl_tmp_imply Config.tbl_tx_imply_c Config.tbl_id_class_atm;
     Printf.sprintf "INSERT IGNORE INTO %s SELECT t1.sub,t2.name FROM %s AS t1 JOIN %s AS t2 ON t1.sup=t2.repr" 
                     Config_output.tbl_tmp_out_imply Config_output.tbl_tmp_imply Config_output.tbl_tmp_repr;
     Printf.sprintf "ANALYZE TABLE %s" Config_output.tbl_tmp_imply;
     Printf.sprintf "DELETE FROM t1 USING %s AS t1 JOIN %s AS t2 ON t1.sup=t2.repr" 
                     Config_output.tbl_tmp_imply Config_output.tbl_tmp_equiv;     
     Printf.sprintf "INSERT IGNORE INTO %s SELECT t1.sub,t2.name FROM %s AS t1 JOIN %s AS t2 ON t1.sup=t2.id" 
                     Config_output.tbl_tmp_out_imply Config_output.tbl_tmp_imply Config.tbl_id_class_atm];
  let endt = Unix.gettimeofday () in Printf.fprintf stderr " done in %fs\n" (endt -. begt); flush stderr;        

  Printf.fprintf stderr "3. Outputing taxonomy..."; flush stderr;    
  
  let ok = ref true in
  let cequiv = ref 0 in
  let cimply = ref 0 in
  let hequiv = ref None in
  let himply = ref None in
  
  let out_channel = (if (String.compare file "") = 0 then stdout else open_out file) in
  
  let rec fetch_next_equiv () = fetch_next hequiv cequiv Config_output.tbl_tmp_out_equiv
  and     fetch_next_imply () = fetch_next himply cimply Config_output.tbl_tmp_out_imply
  and fetch_next handle count table =
    match !handle with
      | Some res ->
      begin
        try
          fetch_couple res
        with | _ -> reload handle count table;
      end
      | None ->     reload handle count table;
  and reload handle count table =
    handle := Some (DB.query db (Printf.sprintf "SELECT * FROM %s ORDER BY sub,sup LIMIT %d,1000000" table !count));
    count := !count + 1000000;
    match !handle with
      | Some res -> begin try fetch_couple res with | _ -> handle := None; ("","") end
      | None ->     ("","")    
  and fetch_couple res =
    match DB.fetch_array res with
      | None -> raise Not_found
      | Some a ->
      begin
        match a.(0),a.(1) with
          | Some s1,Some s2 -> (s1,s2)
          | _ -> raise Not_found
      end      
  in  
  
  let (head,tail) = fetch_next_equiv () in  
  let (sub,sup) = fetch_next_imply () in
  let equiv_head = ref head in
  let equiv_tail = ref tail in
  let imply_sub  = ref sub in
  let imply_sup  = ref sup in  
  
  Printf.fprintf out_channel "Ontology(taxonomy\n";  
  while !ok do
    if !himply<>None && (!hequiv=None || (String.compare !equiv_head !imply_sub) > 0) then
    begin
      Printf.fprintf out_channel "SubClassOf(%s %s)\n" !imply_sub !imply_sup;
      let (sub,sup) = fetch_next_imply () in
      imply_sub := sub;
      imply_sup := sup;
    end
    else
    if !hequiv=None then
      ok := false
    else
    begin
      let head = !equiv_head in 
      Printf.fprintf out_channel "EquivalentClasses(%s" head;   
      while (String.compare head !equiv_head) = 0 do
        Printf.fprintf out_channel " %s" !equiv_tail;
        let (head,tail) = fetch_next_equiv () in
        equiv_head := head;
        equiv_tail := tail;
      done;
      Printf.fprintf out_channel ")\n";
    end
  done;
  Printf.fprintf out_channel ")";  
  
  drop_tmp_tables db;
  Common.set_status db "output" "done" 0
  end
