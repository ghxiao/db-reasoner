/** parser for owl files in functional-style syntax according to the specification on
http://www.w3.org/TR/owl2-syntax/ */

/* non-terminal symbols are prefixed with "_" to be consistent with the notation from 
the specification; the symbol "-" is also replaced with "_". */

%{
open DL
%}

/* structure keywords */
%token LeftParen RightParen Equal LeftAngle RightAngle

/* reserved keywords */
%token Owl_backwardCompatibleWith Owl_bottomDataProperty Owl_bottomObjectProperty Xsd_dateTimeStamp 
%token Owl_deprecated Owl_incompatibleWith Owl_Nothing Owl_priorVersion Owl_rational Owl_real 
%token Owl_Thing Owl_topDataProperty Owl_topObjectProperty Rdf_langPattern Rdf_PlainLiteral 
%token Rdf_XMLLiteral Rdfs_comment Rdfs_isDefinedBy Rdfs_label Rdfs_Literal Rdfs_seeAlso 
%token Xsd_anyURI Xsd_base64Binary Xsd_boolean Xsd_byte Xsd_decimal Xsd_double Xsd_float 
%token Xsd_hexBinary Xsd_int Xsd_integer Xsd_language Xsd_length Xsd_long Xsd_maxExclusive 
%token Xsd_maxInclusive Xsd_maxLength Xsd_minExclusive Xsd_minInclusive Xsd_minLength Xsd_Name 
%token Xsd_NCName Xsd_negativeInteger Xsd_NMTOKEN Xsd_nonNegativeInteger Xsd_nonPositiveInteger 
%token Xsd_normalizedString Xsd_pattern Xsd_positiveInteger Xsd_short Xsd_string Xsd_token 
%token Xsd_unsignedByte Xsd_unsignedInt Xsd_unsignedLong Xsd_unsignedShort

/* ontology keywords */
%token Prefix Ontology Import

/* Integers, Characters, Strings, Language Tags, and Node IDs */
%token <int> NonNegativeInteger 
%token <string> QuotedString 
%token <string> NodeID

/* entities and individuals */ 
%token Class Datatype ObjectProperty DataProperty AnnotationProperty NamedIndividual DoubleSuperscript At

/* declaration */
%token Declaration

/* property expressions */
%token ObjectInverseOf ObjectPropertyChain
  
/* data ranges */
%token ComplementOf OneOf DatatypeRestriction

/* class expressions */ 
%token ObjectIntersectionOf ObjectUnionOf ObjectComplementOf ObjectOneOf ObjectSomeValuesFrom ObjectAllValuesFrom
%token ObjectHasValue ObjectHasSelf ObjectMinCardinality ObjectMaxCardinality ObjectExactCardinality 
%token DataIntersectionOf DataUnionOf DataComplementOf DataOneOf DataSomeValuesFrom DataAllValuesFrom 
%token DataHasValue DataHasSelf DataMinCardinality DataMaxCardinality DataExactCardinality

/* class expressions axioms */
%token SubClassOf EquivalentClasses DisjointClasses DisjointUnion  

/* object property axioms, data property axioms, keys */ 
%token PropertyChain 
%token SubObjectPropertyOf EquivalentObjectProperties DisjointObjectProperties ObjectPropertyDomain 
%token ObjectPropertyRange InverseObjectProperties FunctionalObjectProperty InverseFunctionalObjectProperty 
%token ReflexiveObjectProperty IrreflexiveObjectProperty SymmetricObjectProperty AsymmetricObjectProperty 
%token TransitiveObjectProperty SubDataPropertyOf EquivalentDataProperties DisjointDataProperties DataPropertyDomain 
%token DataPropertyRange InverseDataProperties DatatypeDefinition FunctionalDataProperty InverseFunctionalDataProperty 
%token ReflexiveDataProperty IrreflexiveDataProperty SymmetricDataProperty AsymmetricDataProperty
%token TransitiveDataProperty HasKey 
  
/* assertions */
%token SameIndividual DifferentIndividuals ClassAssertion ObjectPropertyAssertion DataPropertyAssertion 
%token NegativeObjectPropertyAssertion NegativeDataPropertyAssertion
 
/* annotations */  
%token Annotation AnnotationAssertion SubAnnotationPropertyOf AnnotationPropertyDomain AnnotationPropertyRange

/* !DEPRECIATED: ONLY FOR COMPATIBILITY! */
%token EntityAnnotation Label Comment

/* identifiers */
%token <string> Identifier

/* comments */
%token Ignore

/* eof */
%token EOF

/**==========================================================================================**/

%start owl_ontologyDocument
%type <string*DL.load_info> owl_ontologyDocument
%%

/* 2 Preliminary Definitions */

/* 2.3 Integers, Characters, Strings, Language Tags, and Node IDs */

owl_nonNegativeInteger: NonNegativeInteger { $1 }
owl_quotedString: QuotedString             { $1 }
owl_languageTag:    /* specified in BCP 47 [BCP 47] */
  | Identifier                             { $1 }
owl_nodeID: NodeID                         { $1 }


/* 2.3 IRIs */

owl_fullIRI:       /* defined in [RFC3987] */
  | LeftAngle Identifier RightAngle  { "<" ^ $2 ^ ">" }
owl_prefixName:         /* a finite sequence of characters matching the as PNAME_NS production of [SPARQL] */
  | Identifier                 { $1 }
owl_abbreviatedIRI:     /* a finite sequence of characters matching the PNAME_LN production of [SPARQL] */
  | Identifier                 { $1 }  
owl_IRI: 
  | owl_fullIRI               { $1 } 
  | owl_abbreviatedIRI         { $1 }
 
 
/* 3 Ontologies */

/* 3.5 Ontology Annotations */
owl_ontologyAnnotations: /* empty */   {}
  | owl_ontologyAnnotations owl_Annotation {}

/* 3.7 Functional-Style Syntax */
owl_ontologyDocument:   
  | owl_prefixDeclarations owl_Ontology EOF   { $2 }
owl_prefixDeclarations: /* empty */          {}
  | owl_prefixDeclarations owl_prefixDeclaration {}
owl_prefixDeclaration:
  | Prefix LeftParen owl_prefixName Equal owl_fullIRI RightParen {}   
owl_Ontology:
  | Ontology LeftParen owl_directlyImportsDocuments owl_ontologyAnnotations owl_axioms RightParen
    { ("",$5) }
  | Ontology LeftParen owl_ontologyIRI owl_directlyImportsDocuments owl_ontologyAnnotations owl_axioms RightParen
    { ($3,$6) }
  | Ontology LeftParen owl_ontologyIRI owl_versionIRI owl_directlyImportsDocuments owl_ontologyAnnotations 
    owl_axioms RightParen { ($3,$7) } 
owl_ontologyIRI: owl_IRI      { $1 }
owl_versionIRI: owl_IRI       {}
owl_directlyImportsDocuments: /* empty */      { [] }
  | owl_directlyImportsDocuments Import LeftParen owl_IRI RightParen        { $4 :: $1 }
owl_axioms: /* empty */       { DL.init_load () }  
  | owl_axioms owl_Axiom      { let _ = match $2 with | Some c -> DL.add_axiom $1 c | _ -> () in $1 }

/* 4 Datatype Maps */

/* 4.1 Real Numbers, Decimal Numbers, and Integers */
owl_DTRealDecimalIntegers:
  | Owl_real                 {}
  | Owl_rational             {}  
  | Xsd_decimal              {}
  | Xsd_integer              {}
  | Xsd_nonNegativeInteger   {}
  | Xsd_nonPositiveInteger   {}
  | Xsd_positiveInteger      {}
  | Xsd_negativeInteger      {}
  | Xsd_long                 {}
  | Xsd_int                  {}
  | Xsd_short                {}
  | Xsd_byte                 {}
  | Xsd_unsignedLong         {}
  | Xsd_unsignedInt          {}
  | Xsd_unsignedShort        {}
  | Xsd_unsignedByte         {}

/* 4.2 Floating-Point Numbers */
owl_DTFloats:
  | Xsd_double               {}
  | Xsd_float                {}

/* 4.3 Strings */
owl_DTStrings:
  | Xsd_string               {}
  | Xsd_normalizedString     {}
  | Xsd_token                {}
  | Xsd_language             {}
  | Xsd_Name                 {}
  | Xsd_NCName               {}
  | Xsd_NMTOKEN              {}

/* 4.4 Boolean Values */
owl_DTBooleans:
  | Xsd_boolean              {}

/* 4.5 Binary Data */
owl_DTBinaries:
  | Xsd_hexBinary            {}
  | Xsd_base64Binary         {}

/* 4.6 IRIs */
owl_DTIRIs:
  | Xsd_anyURI               {}

/* 4.7 Time Instants */
owl_DTTimes:
  | Xsd_dateTimeStamp        {}

/* 4.8 XML Literals */
owl_DTXMLs:
  | Rdf_XMLLiteral           {}

/* 5 Entities and Literals */

/* 5.1 Classes */
owl_Class:  
  | owl_IRI                  { ATOMIC_C $1 }  
  | Owl_Thing                { NONE_C }
  | Owl_Nothing              { NONE_C }

/* 5.2 Datatypes */
owl_Datatype:
  | owl_IRI                     {}  
  | Rdfs_Literal                {}
  | owl_DTRealDecimalIntegers   {}
  | owl_DTFloats                {}
  | owl_DTStrings               {}
  | owl_DTBooleans              {}
  | owl_DTBinaries              {}
  | owl_DTIRIs                  {}
  | owl_DTTimes                 {}
  | owl_DTXMLs                  {}
  
/* 5.3 Object Properties */   
owl_ObjectProperty: 
  | owl_IRI                  { $1 }
  | Owl_topObjectProperty    { "" }
  | Owl_bottomObjectProperty { "" }

/* 5.4 Data Properties */
owl_DataProperty: 
  | owl_IRI                     {}
  | Owl_topDataProperty      {}
  | Owl_bottomDataProperty   {}

/* 5.5 Annotation Properties */
owl_AnnotationProperty:
  | owl_IRI                     {}
  | Rdfs_label                  {}
  | Rdfs_comment                {}
  | Rdfs_seeAlso                {}
  | Rdfs_isDefinedBy            {}
  | Owl_deprecated              {}
  | Owl_priorVersion            {}
  | Owl_backwardCompatibleWith  {}
  | Owl_incompatibleWith        {}

/* 5.6 Individuals */
owl_Individuals: /* empty */       {}
  | owl_Individuals owl_Individual {}
owl_Individual:
  | owl_NamedIndividual           {} 
  | owl_AnonymousIndividual       {}

/* 5.6.1 Named Individuals */
owl_NamedIndividual: owl_IRI         {}

/* 5.6.2 Anonymous Individuals */
owl_AnonymousIndividual: owl_nodeID  {}

/* 5.7 Literals */
owl_Literals: /* empty */            {}  
  | owl_Literals owl_Literal         {}
owl_Literal: 
  | owl_typedLiteral                 {}
  | owl_abbreviatedXSDStringLiteral  {}
  | owl_abbreviatedRDFTextLiteral    {}
owl_typedLiteral:
  | owl_lexicalForm DoubleSuperscript owl_Datatype {}
owl_lexicalForm: owl_quotedString    {}
owl_abbreviatedXSDStringLiteral: 
  | owl_quotedString                 {}
owl_abbreviatedRDFTextLiteral: 
  | owl_quotedString At owl_languageTag {}

/* 5.8 Entity Declarations and Typing */
owl_Declaration:
  | Declaration LeftParen owl_axiomAnnotations owl_Entity RightParen  {}  
owl_Entity:
  | Class LeftParen owl_Class RightParen                           {}
  | Datatype LeftParen owl_Datatype RightParen                     {}
  | ObjectProperty LeftParen owl_ObjectProperty RightParen         {}
  | DataProperty LeftParen owl_DataProperty RightParen             {}
  | AnnotationProperty LeftParen owl_AnnotationProperty RightParen {}
  | NamedIndividual LeftParen owl_NamedIndividual RightParen       {}

/* 6 Property Expressions */
owl_ObjectPropertyExpressions:  /* empty */                 { [] }
  | owl_ObjectPropertyExpressions owl_ObjectPropertyExpression { $2 :: $1 }
owl_ObjectPropertyExpression:
  | owl_ObjectProperty                 { ATOMIC_R $1 }  
  | owl_InverseObjectProperty          { NONE_R }

/* 6.1.1 Inverse Object Properties */
owl_InverseObjectProperty:
  | ObjectInverseOf LeftParen owl_ObjectProperty RightParen 
    {}

/* 6.2 Data Property Expressions */
owl_DataPropertyExpressions: /* empty */                {}  
  | owl_DataPropertyExpressions owl_DataPropertyExpression {}
owl_DataPropertyExpression:
  | owl_DataProperty                                    {}  

/* 7 Data Ranges */
owl_DataRanges:  /* empty */          {}
  | owl_DataRanges owl_DataRange         {}
owl_DataRange:
  | owl_Datatype                       {}
  | owl_DataIntersectionOf             {}
  | owl_DataUnionOf                    {}
  | owl_DataComplementOf               {} 
  | owl_DataOneOf                      {}
  | owl_DatatypeRestriction            {}

/* 7.1 Intersection of Data Ranges */
owl_DataIntersectionOf:
  | DataIntersectionOf LeftParen owl_DataRange owl_DataRange owl_DataRanges RightParen {}

/* 7.2 Union of Data Ranges */
owl_DataUnionOf:
  | DataUnionOf LeftParen owl_DataRange owl_DataRange owl_DataRanges RightParen  {}

/* 7.3 Complement of Data Ranges */
owl_DataComplementOf:
  | DataComplementOf LeftParen owl_DataRange RightParen {}

/* 7.4 Enumeration of Literals */
owl_DataOneOf:
  | DataOneOf LeftParen owl_Literal owl_Literals RightParen {}

/* 7.5 Datatype Restrictions */
owl_DatatypeRestriction: 
  | DatatypeRestriction LeftParen owl_Datatype owl_constrainingFacet owl_restrictionValue owl_constrainingFacet_restrictionValues RightParen {}
owl_constrainingFacet_restrictionValues:  /* empty */                            {}  
  | owl_constrainingFacet_restrictionValues owl_constrainingFacet owl_restrictionValue {}
owl_constrainingFacet: owl_IRI    {}
owl_restrictionValue: owl_Literal {}

/* 8 Class Expressions */
owl_ClassExpressions:  /* empty */         { [] }
  | owl_ClassExpressions owl_ClassExpression  { $2 :: $1 }
owl_ClassExpression:  
  | owl_Class                   { $1 }  
  | owl_ObjectIntersectionOf    { $1 }
  | owl_ObjectUnionOf           { NONE_C }
  | owl_ObjectComplementOf      { NONE_C }
  | owl_ObjectOneOf             { NONE_C }
  | owl_ObjectSomeValuesFrom    { $1 }
  | owl_ObjectAllValuesFrom     { NONE_C }
  | owl_ObjectHasValue          { NONE_C }
  | owl_ObjectHasSelf           { NONE_C }
  | owl_ObjectMinCardinality    { NONE_C }
  | owl_ObjectMaxCardinality    { NONE_C }
  | owl_ObjectExactCardinality  { NONE_C }
  | owl_DataSomeValuesFrom      { NONE_C }
  | owl_DataAllValuesFrom       { NONE_C }
  | owl_DataHasValue            { NONE_C }
  | owl_DataMinCardinality      { NONE_C }
  | owl_DataMaxCardinality      { NONE_C }
  | owl_DataExactCardinality    { NONE_C }

/* 8.1 Propositional Connectives and Enumeration of Individuals */

/* 8.1.1 Intersection of Class Expressions */
owl_ObjectIntersectionOf: 
  | ObjectIntersectionOf LeftParen owl_ClassExpression owl_ClassExpression owl_ClassExpressions RightParen 
    { INTER_RESTR ($3 :: $4 :: $5) }

/* 8.1.2 Union of Class Expressions */
owl_ObjectUnionOf:
  | ObjectUnionOf LeftParen owl_ClassExpression owl_ClassExpression owl_ClassExpressions RightParen 
    {}
 
/* 8.1.3 Complement of Class Expressions */
owl_ObjectComplementOf:
  | ObjectComplementOf LeftParen owl_ClassExpression RightParen 
    {}

/* 8.1.4 Enumeration of Individuals */
owl_ObjectOneOf: 
  | ObjectOneOf LeftParen owl_Individual owl_Individuals RightParen 
    {}

/* 8.2 Object Property Restrictions */

/* 8.2.1 Existential Quantification */
owl_ObjectSomeValuesFrom:
  | ObjectSomeValuesFrom LeftParen owl_ObjectPropertyExpression owl_ClassExpression RightParen 
    { EXIST_RESTR ($3 ,$4) }
 
/* 8.2.2 Universal Quantification */
owl_ObjectAllValuesFrom:
  | ObjectAllValuesFrom LeftParen owl_ObjectPropertyExpression owl_ClassExpression RightParen 
    {}

/* 8.2.3 Individual Value Restriction */
owl_ObjectHasValue:
  | ObjectHasValue LeftParen owl_ObjectPropertyExpression owl_Individual RightParen 
    {}

/* 8.2.4 Self-Restriction */
owl_ObjectHasSelf: 
  | ObjectHasSelf LeftParen owl_ObjectPropertyExpression RightParen  
    {}

/* 8.3 Object Property Cardinality Restrictions */

/* 8.3.1 Minimum Cardinality */
owl_ObjectMinCardinality:
  | ObjectMinCardinality LeftParen owl_nonNegativeInteger owl_ObjectPropertyExpression RightParen  
    {}
  | ObjectMinCardinality LeftParen owl_nonNegativeInteger owl_ObjectPropertyExpression owl_ClassExpression RightParen 
    {}

/* 8.3.2 Maximum Cardinality */
owl_ObjectMaxCardinality:
  | ObjectMaxCardinality LeftParen owl_nonNegativeInteger owl_ObjectPropertyExpression RightParen 
    {}
  | ObjectMaxCardinality LeftParen owl_nonNegativeInteger owl_ObjectPropertyExpression owl_ClassExpression RightParen 
    {}

/* 8.3.3 Exact Cardinality */
owl_ObjectExactCardinality:
  | ObjectExactCardinality LeftParen owl_nonNegativeInteger owl_ObjectPropertyExpression RightParen 
    {}
  | ObjectExactCardinality LeftParen owl_nonNegativeInteger owl_ObjectPropertyExpression owl_ClassExpression RightParen 
    {}

/* 8.4 Data Property Restrictions */

/* 8.4.1 Existential Quantification */
owl_DataSomeValuesFrom:
  | DataSomeValuesFrom LeftParen owl_DataPropertyExpression owl_DataPropertyExpressions owl_DataRange RightParen 
    {}

/* 8.4.2 Universal Quantification */
owl_DataAllValuesFrom:
  | DataAllValuesFrom LeftParen owl_DataPropertyExpression owl_DataPropertyExpressions owl_DataRange RightParen 
    {}

/* 8.4.3 Literal Value Restriction */
owl_DataHasValue:
  | DataHasValue LeftParen owl_DataPropertyExpression owl_Literal RightParen 
    {}

/* 8.5 Data Property Cardinality Restrictions */

/* 8.5.1 Minimum Cardinality */
owl_DataMinCardinality:
  | DataMinCardinality LeftParen owl_nonNegativeInteger owl_DataPropertyExpression RightParen 
    {}
  | DataMinCardinality LeftParen owl_nonNegativeInteger owl_DataPropertyExpression owl_DataRange RightParen 
    {}

/* 8.5.2 Maximum Cardinality */
owl_DataMaxCardinality:
  | DataMaxCardinality LeftParen owl_nonNegativeInteger owl_DataPropertyExpression RightParen 
    {}
  | DataMaxCardinality LeftParen owl_nonNegativeInteger owl_DataPropertyExpression owl_DataRange RightParen 
    {} 

/* 8.5.3 Exact Cardinality */
owl_DataExactCardinality:
  | DataExactCardinality LeftParen owl_nonNegativeInteger owl_DataPropertyExpression RightParen 
    {}
  | DataExactCardinality LeftParen owl_nonNegativeInteger owl_DataPropertyExpression owl_DataRange RightParen 
    {}

/* 9 Axioms */
owl_Axiom:
  | owl_Declaration         { None }
  | owl_ClassAxiom          { $1 }
  | owl_ObjectPropertyAxiom { $1 }
  | owl_DataPropertyAxiom   { Printf.fprintf stderr "Ignored DataPropertyAxiom\n"; None }
  | owl_DatatypeDefinition  { Printf.fprintf stderr "Ignored DatatypeDefinition\n"; None }
  | owl_HasKey              { Printf.fprintf stderr "Ignored HasKey\n"; None }
  | owl_Assertion           { Printf.fprintf stderr "Ignored Assertion\n"; None }
  | owl_AnnotationAxiom     { None }
owl_axiomAnnotations: /* empty */    {}
  | owl_axiomAnnotations owl_Annotation {}

/* 9.1 Class Expression Axioms */
owl_ClassAxiom:
  | owl_SubClassOf        { $1 }
  | owl_EquivalentClasses { $1 }
  | owl_DisjointClasses   { $1 }
  | owl_DisjointUnion     { $1 }

/* 9.1.1 Subclass Axioms */
owl_SubClassOf:
  | SubClassOf LeftParen owl_axiomAnnotations owl_subClassExpression owl_superClassExpression RightParen 
    { Some (CONC_INCL ($4, $5)) }
owl_subClassExpression: owl_ClassExpression   { $1 }
owl_superClassExpression: owl_ClassExpression { $1 }

/* 9.1.2 Equivalent Classes */
owl_EquivalentClasses:
  | EquivalentClasses LeftParen owl_axiomAnnotations owl_ClassExpression owl_ClassExpression owl_ClassExpressions RightParen 
    { Some (CONC_EQUI ($4 :: $5 :: $6)) }
    
/* 9.1.3 Disjoint Classes */
owl_DisjointClasses:
  | DisjointClasses LeftParen owl_axiomAnnotations owl_ClassExpression owl_ClassExpression owl_ClassExpressions RightParen 
    { Printf.fprintf stderr "Ignored DisjointClasses\n"; None }
    
/* 9.1.4 Disjoint Union of Class Expressions */
owl_DisjointUnion:
  | DisjointUnion LeftParen owl_axiomAnnotations owl_Class owl_disjointClassExpressions RightParen 
    { Printf.fprintf stderr "Ignored DisjointUnion\n"; None }
owl_disjointClassExpressions:
  | owl_ClassExpression owl_ClassExpression owl_ClassExpressions {}

/* 9.2 Object Property Axioms */
owl_ObjectPropertyAxiom:
  | owl_SubObjectPropertyOf             { $1 }
  | owl_EquivalentObjectProperties      { $1 }
  | owl_DisjointObjectProperties        { $1 }
  | owl_InverseObjectProperties         { $1 }
  | owl_ObjectPropertyDomain            { $1 }
  | owl_ObjectPropertyRange             { $1 }
  | owl_FunctionalObjectProperty        { $1 }
  | owl_InverseFunctionalObjectProperty { $1 }
  | owl_ReflexiveObjectProperty         { $1 }
  | owl_IrreflexiveObjectProperty       { $1 }
  | owl_SymmetricObjectProperty         { $1 }
  | owl_AsymmetricObjectProperty        { $1 }
  | owl_TransitiveObjectProperty        { $1 }  

/* 9.2.1 Object Subproperties */
owl_SubObjectPropertyOf:
  | SubObjectPropertyOf LeftParen owl_axiomAnnotations owl_subObjectPropertyExpression owl_superObjectPropertyExpression RightParen 
    { Some (ROLE_INCL ($4,$5)) }
owl_subObjectPropertyExpression:
  | owl_ObjectPropertyExpression { $1 }
  | owl_propertyExpressionChain  { $1 }
owl_propertyExpressionChain:
  | ObjectPropertyChain LeftParen owl_ObjectPropertyExpression owl_ObjectPropertyExpression owl_ObjectPropertyExpressions RightParen 
    { Printf.fprintf stderr "Ignored ObjectPropertyChain\n"; NONE_R }
owl_superObjectPropertyExpression: owl_ObjectPropertyExpression { $1 }

/* 9.2.2 Equivalent Object Properties */
owl_EquivalentObjectProperties:
  | EquivalentObjectProperties LeftParen owl_axiomAnnotations owl_ObjectPropertyExpression owl_ObjectPropertyExpression 
    owl_ObjectPropertyExpressions RightParen 
    { Some (ROLE_EQUI ($4 :: $5 :: $6)) }

/* 9.2.3 Disjoint Object Properties */
owl_DisjointObjectProperties:
  | DisjointObjectProperties LeftParen owl_axiomAnnotations owl_ObjectPropertyExpression owl_ObjectPropertyExpression 
    owl_ObjectPropertyExpressions RightParen 
    { Printf.fprintf stderr "Ignored DisjointObjectProperties\n"; None }

/* 9.2.4 Inverse Object Properties */
owl_InverseObjectProperties:
  | InverseObjectProperties LeftParen owl_axiomAnnotations owl_ObjectPropertyExpression owl_ObjectPropertyExpression RightParen 
    { Printf.fprintf stderr "Ignored InverseObjectProperties\n"; None }

/* 9.2.5 Object Property Domain */
owl_ObjectPropertyDomain: 
  | ObjectPropertyDomain LeftParen owl_axiomAnnotations owl_ObjectPropertyExpression owl_ClassExpression RightParen 
    { Printf.fprintf stderr "Ignored ObjectPropertyDomain\n"; None }

/* 9.2.6 Object Property Range */
owl_ObjectPropertyRange: 
  | ObjectPropertyRange LeftParen owl_axiomAnnotations owl_ObjectPropertyExpression owl_ClassExpression RightParen 
    { Printf.fprintf stderr "Ignored ObjectPropertyRange\n"; None }

/* 9.2.7 Functional Object Properties */
owl_FunctionalObjectProperty: 
  | FunctionalObjectProperty LeftParen owl_axiomAnnotations owl_ObjectPropertyExpression RightParen 
    { Printf.fprintf stderr "Ignored FunctionalObjectProperty\n"; None }

/* 9.2.8 Inverse-Functional Object Properties */
owl_InverseFunctionalObjectProperty: 
  | InverseFunctionalObjectProperty LeftParen owl_axiomAnnotations owl_ObjectPropertyExpression RightParen 
    { Printf.fprintf stderr "Ignored InverseFunctionalObjectProperty\n"; None }

/* 9.2.9 Reflexive Object Properties */
owl_ReflexiveObjectProperty: 
  | ReflexiveObjectProperty LeftParen owl_axiomAnnotations owl_ObjectPropertyExpression RightParen 
    { Printf.fprintf stderr "Ignored ReflexiveObjectProperty\n"; None }

/* 9.2.10 Irreflexive Object Properties */
owl_IrreflexiveObjectProperty: 
  | IrreflexiveObjectProperty LeftParen owl_axiomAnnotations owl_ObjectPropertyExpression RightParen 
    { Printf.fprintf stderr "Ignored IrreflexiveObjectProperty\n"; None }
    
/* 9.2.11 Symmetric Object Properties */    
owl_SymmetricObjectProperty: 
  | SymmetricObjectProperty LeftParen owl_axiomAnnotations owl_ObjectPropertyExpression RightParen 
    { Printf.fprintf stderr "Ignored SymmetricObjectProperty\n"; None }

/* 9.2.12 Asymmetric Object Properties */
owl_AsymmetricObjectProperty: 
  | AsymmetricObjectProperty LeftParen owl_axiomAnnotations owl_ObjectPropertyExpression RightParen 
    { Printf.fprintf stderr "Ignored AsymmetricObjectProperty\n"; None }

/* 9.2.13 Transitive Object Properties */
owl_TransitiveObjectProperty: 
  | TransitiveObjectProperty LeftParen owl_axiomAnnotations owl_ObjectPropertyExpression RightParen 
    { Printf.fprintf stderr "Ignored TransitiveObjectProperty\n"; None }

/* 9.3 Data Property Axioms */
owl_DataPropertyAxiom:
  | owl_SubDataPropertyOf        {}
  | owl_EquivalentDataProperties {}
  | owl_DisjointDataProperties   {}
  | owl_DataPropertyDomain       {}
  | owl_DataPropertyRange        {}
  | owl_FunctionalDataProperty   {}

/* 9.3.1 Data Subproperties */
owl_SubDataPropertyOf:
  | SubDataPropertyOf LeftParen owl_axiomAnnotations owl_subDataPropertyExpression owl_superDataPropertyExpression RightParen {}
owl_subDataPropertyExpression: owl_DataPropertyExpression   {} 
owl_superDataPropertyExpression: owl_DataPropertyExpression {}

/* 9.3.2 Equivalent Data Properties */
owl_EquivalentDataProperties:
  | EquivalentDataProperties LeftParen owl_axiomAnnotations owl_DataPropertyExpression owl_DataPropertyExpression owl_DataPropertyExpressions RightParen {}

/* 9.3.3 Disjoint Data Properties */
owl_DisjointDataProperties:
  | DisjointDataProperties LeftParen owl_axiomAnnotations owl_DataPropertyExpression owl_DataPropertyExpression owl_DataPropertyExpressions RightParen {}

/* 9.3.4 Data Property Domain */
owl_DataPropertyDomain: 
  | DataPropertyDomain LeftParen owl_axiomAnnotations owl_DataPropertyExpression owl_ClassExpression RightParen {}

/* 9.3.5 Data Property Range */
owl_DataPropertyRange: 
  | DataPropertyRange LeftParen owl_axiomAnnotations owl_DataPropertyExpression owl_DataRange RightParen {}

/* 9.3.6 Functional Data Properties */
owl_FunctionalDataProperty: FunctionalDataProperty LeftParen owl_axiomAnnotations owl_DataPropertyExpression RightParen {}

/* 9.4 Datatype Definitions */
owl_DatatypeDefinition:
  | DatatypeDefinition LeftParen owl_axiomAnnotations owl_Datatype owl_DataRange RightParen {}

/* 9.5 Keys */
owl_HasKey:
  | HasKey LeftParen owl_axiomAnnotations owl_ClassExpression 
    LeftParen owl_ObjectPropertyExpressions RightParen LeftParen owl_DataPropertyExpression RightParen {}

/* 9.6 Assertions */
owl_Assertion:
  | owl_SameIndividual                  {}
  | owl_DifferentIndividuals            {}
  | owl_ClassAssertion                  {}
  | owl_ObjectPropertyAssertion         {}
  | owl_NegativeObjectPropertyAssertion {}
  | owl_DataPropertyAssertion           {}
  | owl_NegativeDataPropertyAssertion   {}
owl_sourceIndividual: owl_Individual { $1 }
owl_targetIndividual: owl_Individual { $1 }
owl_targetValue: owl_Literal {}

/* 9.6.1 Individual Equality */
owl_SameIndividual: 
  | SameIndividual LeftParen owl_axiomAnnotations owl_Individual owl_Individual owl_Individuals RightParen 
    {}

/* 9.6.2 Individual Inequality */
owl_DifferentIndividuals: 
  | DifferentIndividuals LeftParen owl_axiomAnnotations owl_Individual owl_Individual owl_Individuals RightParen 
    {}

/* 9.6.3 Class Assertions */
owl_ClassAssertion: 
  | ClassAssertion LeftParen owl_axiomAnnotations owl_ClassExpression owl_Individual RightParen 
    {}

/* 9.6.4 Positive Object Property Assertions */
owl_ObjectPropertyAssertion: 
  | ObjectPropertyAssertion LeftParen owl_axiomAnnotations owl_ObjectPropertyExpression owl_sourceIndividual owl_targetIndividual RightParen 
    {}

/* 9.6.5 Negative Object Property Assertions */
owl_NegativeObjectPropertyAssertion: 
  | NegativeObjectPropertyAssertion LeftParen owl_axiomAnnotations owl_ObjectPropertyExpression owl_sourceIndividual 
    owl_targetIndividual RightParen 
    {}

/* 9.6.6 Positive Data Property Assertions */
owl_DataPropertyAssertion: 
  | DataPropertyAssertion LeftParen owl_axiomAnnotations owl_DataPropertyExpression owl_sourceIndividual owl_targetValue RightParen 
    {}

/* 9.6.7 Negative Data Property Assertions */
owl_NegativeDataPropertyAssertion: 
  | NegativeDataPropertyAssertion LeftParen owl_axiomAnnotations owl_DataPropertyExpression owl_sourceIndividual owl_targetValue RightParen 
    {}

/* 10 Annotations */

/* 10.1 Annotations of Ontologies, Axioms, and other Annotations */
owl_Annotations: /* empty */   {}
  | owl_Annotations owl_Annotation {}
owl_Annotation:
  | Annotation LeftParen owl_annotationAnnotations owl_AnnotationProperty owl_AnnotationValue RightParen {}
/* !DEPRECATED: ONLY FOR COMPATIBILITY! */
  | Label LeftParen owl_Literal RightParen {}
  | Comment LeftParen owl_Literal RightParen {} 

owl_annotationAnnotations: owl_Annotations {}
owl_AnnotationValue: 
  | owl_AnonymousIndividual {}
  | owl_IRI                 {}
  | owl_Literal             {}

/* 10.2 Annotation Axioms */
owl_AnnotationAxiom:
  | owl_AnnotationAssertion      {}
  | owl_SubAnnotationPropertyOf  {}
  | owl_AnnotationPropertyDomain {}
  | owl_AnnotationPropertyRange  {}

/* 10.2.1 Annotation Assertion */
owl_AnnotationAssertion:
  | AnnotationAssertion LeftParen owl_axiomAnnotations owl_AnnotationProperty owl_AnnotationSubject owl_AnnotationValue RightParen {}
/* !DEPRECATED: ONLY FOR COMPATIBILITY! */  
  | EntityAnnotation LeftParen owl_Entity owl_Annotation RightParen {}
owl_AnnotationSubject:
  | owl_IRI                 {}
  | owl_AnonymousIndividual {}

/* 10.2.2 Annotation Subproperties */
owl_SubAnnotationPropertyOf:
  | SubAnnotationPropertyOf LeftParen owl_axiomAnnotations owl_subAnnotationProperty owl_superAnnotationProperty RightParen {}
owl_subAnnotationProperty: owl_AnnotationProperty {}
owl_superAnnotationProperty: owl_AnnotationProperty {}  

/* 10.2.3 Annotation Property Domain */
owl_AnnotationPropertyDomain: AnnotationPropertyDomain LeftParen owl_axiomAnnotations owl_AnnotationProperty owl_IRI RightParen {}

/* 10.2.4 Annotation Property Range */
owl_AnnotationPropertyRange: AnnotationPropertyRange LeftParen owl_axiomAnnotations owl_AnnotationProperty owl_IRI RightParen {}
