
let tbl_tmp_tx_atm_only        = "tmp_tx_atm_only"
let tbl_tmp_tx_atm_only_create = Printf.sprintf "CREATE TABLE %s (sub INT NOT NULL,\
                                                                  sup INT NOT NULL\
                                                                 )" tbl_tmp_tx_atm_only                                                                

let tbl_tmp_tx_queue        = "tmp_tx_queue"
let tbl_tmp_tx_queue_create = Printf.sprintf "CREATE TABLE %s (id INT NOT NULL,\
                                                               PRIMARY KEY (id)\
                                                              )" tbl_tmp_tx_queue

let tbl_tmp_tx_mem        = "tmp_tx_mem"
let tbl_tmp_tx_mem_create = Printf.sprintf "CREATE TABLE %s (sub INT NOT NULL,\
                                                             sup INT NOT NULL,\
                                                             direct BOOL NOT NULL,\
                                                             PRIMARY KEY (sub,sup)\
                                                            ) ENGINE=MEMORY" tbl_tmp_tx_mem                                                                  
                                                                 

