open DB_sig
open DB_mysql

module DB = DBmysql;;

(**============ main function, dealing with parameters ==============**)

type action = NONE_ACT | ALL | RESUME | LOAD | CLOSURE | CLASSIFY | OUTPUT

let main _ = 
  let act = ref NONE_ACT in
  let special_flag = ref Common.FLnone in
  let filename = ref "" in
  let outfile = ref "" in  
  
  let set_action a =
    if !special_flag = Common.FLresume then 
      (Printf.fprintf stderr "You cannot use -r and specify an action. The flag -r will resume the last stopped action.\n"; flush stderr)
    else act := a
  in
  
  Arg.parse ["-d",Arg.String(fun db -> Common.database := db),
                "Set the database to use";
             "-a",Arg.String(fun f -> filename := f; set_action ALL),
                "Load and classify the ontology, output the taxonomy";
             "-l",Arg.String(fun f -> filename := f; set_action LOAD),
                "Load the ontology";                
             "-c",Arg.Unit(fun () -> set_action CLOSURE),
                "Compute the transitive closure";                
             "-t",Arg.Unit(fun () -> set_action CLASSIFY),
                "Compute the classification";     
             "-o",Arg.Unit(fun () -> set_action OUTPUT),
                "Output the classification";                             
             "-x",Arg.String(fun f -> outfile := f),
                "Specify the output file. If omitted, output is done on stdout";                
             "-f",Arg.Unit(fun () -> 
               if !special_flag <> Common.FLresume then special_flag := Common.FLforce),
                "Force to update tables";              
             "-r",Arg.Unit(fun () -> 
               if !act = NONE_ACT then (special_flag := Common.FLresume; act := RESUME)
                                  else (Printf.fprintf stderr "You cannot use -r and specify an action. The flag -r will resume the last stopped action.\n"; flush stderr)),
                "Resume the work in progress";                                           
 	          ]
    (fun str -> prerr_string "Ignored argument (try -help for some help): ";prerr_string str;prerr_newline())
    "DB - Vincent Delaitre";
  if (String.compare !Common.database "") = 0 then
    Printf.fprintf stderr "No database defined, use option -d database_name to indicate which database to use.\n"
  else 
  begin  
    let begt = Unix.gettimeofday () in   
    begin
    let db = DB.connect (Some Access.server) (Some Access.port) (Some Access.user) (Some Access.password) None in    
    DB.query_list db false [
      Printf.sprintf "CREATE DATABASE IF NOT EXISTS %s DEFAULT CHARACTER SET utf8 COLLATE utf8_bin" !Common.database;
      Printf.sprintf "CREATE TABLE IF NOT EXISTS %s.%s %s" !Common.database Config.tbl_info Config.tbl_info_create;
      Printf.sprintf "INSERT IGNORE INTO %s.%s VALUES ('filename','',0),('ontologyIRI','',0),('load','',0),('classify','',0),('taxonomy','',0),('output','',0)"
                      !Common.database Config.tbl_info];                 
    match !act with
      | NONE_ACT -> Printf.fprintf stderr "No action specified, try --help for some help\n"; flush stderr;
      | ALL      -> Load.load !filename !special_flag;
                    Classify.classify !special_flag;
                    Taxonomy.taxonomy !special_flag;  
                    Output.output !outfile 
      | RESUME   -> let db = Common.open_connection () in 
                    let (s,_) = Common.get_status db "load" in
                    if (String.compare s "done") <> 0 then (Load.load "" !special_flag; special_flag:=Common.FLforce);
                    let (s,_) = Common.get_status db "classify" in                    
                    if (String.compare s "done") <> 0 then (Classify.classify !special_flag; special_flag:=Common.FLforce);                    
                    let (s,_) = Common.get_status db "taxonomy" in                     
                    if (String.compare s "done") <> 0 then (Taxonomy.taxonomy !special_flag);                    
                    Output.output !outfile                    
      | LOAD     -> Load.load !filename !special_flag
      | CLOSURE  -> Classify.classify !special_flag
      | CLASSIFY -> Taxonomy.taxonomy !special_flag  
      | OUTPUT   -> Output.output !outfile
    end;
    let endt = Unix.gettimeofday () in Printf.fprintf stderr "\ndone in %fs\n" (endt -. begt);  flush stderr
  end;      
;;
  
main ();;
