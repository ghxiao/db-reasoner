
let tbl_tmp_out_imply        = "tmp_out_imply"
let tbl_tmp_out_imply_create = Printf.sprintf "CREATE TABLE %s (sub VARCHAR(10) NOT NULL,\
                                                                sup VARCHAR(10) NOT NULL,\
                                                                INDEX (sub,sup)\
                                                               )" tbl_tmp_out_imply                                                                     

let tbl_tmp_out_equiv        = "tmp_out_equiv"
let tbl_tmp_out_equiv_create = Printf.sprintf "CREATE TABLE %s (sub VARCHAR(10) NOT NULL,\
                                                                sup VARCHAR(10) NOT NULL,\
                                                                INDEX (sub,sup)\
                                                               )" tbl_tmp_out_equiv
                                                               
let tbl_tmp_imply        = "tmp_imply"
let tbl_tmp_imply_create = Printf.sprintf "CREATE TABLE %s (sub VARCHAR(10) NOT NULL,\
                                                            sub_c INT NOT NULL,\
                                                            sup INT NOT NULL,\
                                                            PRIMARY KEY (sup,sub_c)\
                                                           )" tbl_tmp_imply

let tbl_tmp_equiv        = "tmp_equiv"
let tbl_tmp_equiv_create = Printf.sprintf "CREATE TABLE %s (repr INT NOT NULL,\
                                                            id VARCHAR(10) NOT NULL,\
                                                            INDEX (repr)\
                                                           )" tbl_tmp_equiv         

let tbl_tmp_repr        = "tmp_repr"
let tbl_tmp_repr_create = Printf.sprintf "CREATE TABLE %s (repr INT NOT NULL,\
                                                           name VARCHAR(10) NOT NULL\
                                                          )" tbl_tmp_repr
