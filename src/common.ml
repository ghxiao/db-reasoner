open DB_mysql
module DB = DBmysql

let database = ref ""

(**============ Flags Force or Resume ==============**)
type command_flag = FLforce | FLresume | FLnone

(**============ Open connection to database ==============**)

let open_connection () = DB.connect (Some Access.server) (Some Access.port) (Some Access.user) (Some Access.password) (Some !database)

(**============ Load a file and execute its content as an SQL query ==============**)

let exec_script db script = 
  let ic = open_in script in
  let len = ref (in_channel_length ic) in
  let buff = ref (String.make !len ' ') in
  let _ = input ic !buff 0 !len in
  let d = !buff.[0] in
  let res = ref None in
  len := !len-1;
  buff := String.sub !buff 1 !len;
  while (String.compare !buff "") <> 0 do
    let (query,nbuff) = 
      try 
        let i = String.index !buff d in
        len := !len-1-i;
        (String.sub !buff 0 i,String.sub !buff (i+1) !len);
      with | _ -> (!buff,"")
    in
    res := Some (DB.query db query);
    buff := nbuff
  done;
  match !res with
  | None -> raise (DB_sig.DBError DB_sig.Empty_query)
  | Some res -> res
  
let set_status db operation status value =
  let _ = DB.query db (Printf.sprintf "UPDATE %s SET value_text=%S,value_int=%d WHERE type=%S" Config.tbl_info status value operation) in ()
 
let get_status db operation =
  let res = DB.query db (Printf.sprintf "SELECT value_text,value_int FROM %s WHERE type=%S" Config.tbl_info operation) in
  match DB.fetch_array res with
    | None -> "",0
    | Some a ->
    begin
      match a.(0),a.(1) with
        | Some s,Some d -> s,(int_of_string d)
        | _ -> "",0
    end
