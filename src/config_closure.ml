(** Temporary tables for closure **)

let tbl_tmp_axioms        = "tmp_axioms"
let tbl_tmp_axioms_create = Printf.sprintf "CREATE TABLE %s (sub INT NOT NULL,\
                                                             sup INT NOT NULL,\
                                                             PRIMARY KEY (sub,sup)\
                                                            )" tbl_tmp_axioms                                                        

let tbl_tmp_queue        = "tmp_queue"
let tbl_tmp_queue_create = Printf.sprintf "CREATE TABLE %s (ord INT NOT NULL auto_increment,\
                                                            id INT NOT NULL,\
                                                            PRIMARY KEY (ord),\
                                                            UNIQUE (id)\
                                                           )" tbl_tmp_queue

let tbl_tmp_part        = "tmp_part"
let tbl_tmp_part_create = Printf.sprintf "CREATE TABLE %s (id INT NOT NULL)" tbl_tmp_part
                                                          
let tbl_tmp_tc_o        = "tmp_tc_o"
let tbl_tmp_tc_o_create = Printf.sprintf "CREATE TABLE %s (sub INT NOT NULL,\
                                                           sup INT NOT NULL,\
                                                           length INT NOT NULL,\                                                     
                                                           PRIMARY KEY (sub,sup),\
                                                           INDEX (length)\                                                                  
                                                          ) ENGINE=MEMORY" tbl_tmp_tc_o

let tbl_tmp_tc_c        = "tmp_tc_c"
let tbl_tmp_tc_c_create = Printf.sprintf "CREATE TABLE %s (sub INT NOT NULL,\
                                                           sup INT NOT NULL,\
                                                           length INT NOT NULL,\
                                                           PRIMARY KEY (sub,sup),\ 
                                                           INDEX (sub),\
                                                           INDEX USING BTREE (length)\
                                                          ) ENGINE=MEMORY" tbl_tmp_tc_c                                                          

let tbl_tmp_ext_pos        = "tmp_ext_pos"
let tbl_tmp_ext_pos_create = Printf.sprintf "CREATE TABLE %s (sub INT NOT NULL,\
                                                              sup_o INT NOT NULL,\
                                                              sup_c INT NOT NULL,\
                                                              PRIMARY KEY (sup_c,sup_o),\
                                                              INDEX (sub)\
                                                             )" tbl_tmp_ext_pos
                                                               
let tbl_tmp_ext_pos_part        = "tmp_ext_pos_part"
let tbl_tmp_ext_pos_part_create = Printf.sprintf "CREATE TABLE %s LIKE %s" tbl_tmp_ext_pos_part tbl_tmp_ext_pos

let tbl_tmp_ext_neg        = "tmp_ext_neg"
let tbl_tmp_ext_neg_create = Printf.sprintf "CREATE TABLE %s (sub_o INT NOT NULL,\
                                                              sub_c INT NOT NULL,\
                                                              sup INT NOT NULL,\
                                                              PRIMARY KEY (sub_c,sub_o,sup)\
                                                             )" tbl_tmp_ext_neg                                                               

let tbl_tmp_ext_tc        = "tmp_ext_tc"
let tbl_tmp_ext_tc_create = Printf.sprintf "CREATE TABLE %s (sub INT NOT NULL,\
                                                             sup INT NOT NULL\
                                                            )" tbl_tmp_ext_tc                                                               

let tbl_tmp_ext_tc_by_left        = "tmp_ext_tc_by_left"
let tbl_tmp_ext_tc_by_left_create = Printf.sprintf "CREATE TABLE %s (sub INT NOT NULL,\
                                                                     sup INT NOT NULL,\
                                                                     PRIMARY KEY (sub,sup)\
                                                                    )" tbl_tmp_ext_tc_by_left                                                        

