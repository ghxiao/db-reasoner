open DL
open DB_mysql

module DB = DBmysql

(** Drops output tables **) 
let drop_tables db =
  DB.drop_table db Config.tbl_id_object_atm;
  DB.drop_table db Config.tbl_id_class_atm;
  DB.drop_table db Config.tbl_id_class_int;
  DB.drop_table db Config.tbl_id_class_ext;
  DB.drop_table db Config.tbl_ax_object;
  DB.drop_table db Config.tbl_ax_class;
  DB.drop_table db Config.tbl_ax_class_compl

(** Drops temporary output tables **) 
let drop_tmp_tables db =  
  DB.drop_table db Config_load.tbl_tmp_ids;
  DB.drop_table db Config_load.tbl_tmp_c_ax;
  DB.drop_table db Config_load.tbl_tmp_o_ax;
  DB.drop_table db Config_load.tbl_tmp_c_eq;
  DB.drop_table db Config_load.tbl_tmp_o_eq;
  DB.drop_table db Config_load.tbl_tmp_o_atm;
  DB.drop_table db Config_load.tbl_tmp_c_atm;
  DB.drop_table db Config_load.tbl_tmp_c_int;
  DB.drop_table db Config_load.tbl_tmp_c_ext;
  DB.drop_table db Config_load.tbl_tmp_new_eq
      
(** Creates output tables **) 
let create_tables db =
  DB.query_list db false [
    Config.tbl_id_object_atm_create;
    Config.tbl_id_class_atm_create;
    Config.tbl_id_class_int_create;
    Config.tbl_id_class_ext_create;
    Config.tbl_ax_object_create;
    Config.tbl_ax_class_create;
    Config.tbl_ax_class_compl_create;
    Config_load.tbl_tmp_ids_create;
    Config_load.tbl_tmp_c_ax_create;
    Config_load.tbl_tmp_o_ax_create;
    Config_load.tbl_tmp_c_eq_create;
    Config_load.tbl_tmp_o_eq_create;    
    Config_load.tbl_tmp_o_atm_create;
    Config_load.tbl_tmp_c_atm_create;
    Config_load.tbl_tmp_c_int_create;
    Config_load.tbl_tmp_c_ext_create;
    Config_load.tbl_tmp_new_eq_create]
    
(**============ Update atomic ids ==============**)                                                  
let update_atomic db tbl_ids tbl_tmp tbl_dest =
  DB.query_list db false [
    Printf.sprintf "INSERT IGNORE INTO %s (id,real_id) (SELECT id,id FROM %s)" tbl_ids tbl_dest;  
    Printf.sprintf "INSERT IGNORE INTO %s (SELECT id,name,hash FROM %s)" tbl_dest tbl_tmp;
    Printf.sprintf "INSERT INTO %s (id,real_id) (SELECT t1.id,t2.id FROM %s AS t1, %s AS t2 USE INDEX (hash) \
                    WHERE t1.hash=t2.hash AND t1.name=t2.name)" tbl_ids tbl_tmp tbl_dest;
    Printf.sprintf "DROP TABLE %s" tbl_tmp]

(**============ Update composed ids ==============**)   
let update_composed db tbl_ids tbl_ids_left tbl_ids_right tbl_from tbl_real tbl_tmp tbl_dest =
  DB.query_list db false [       
    Printf.sprintf "INSERT INTO %s (SELECT t1.id,t2.real_id,t3.real_id,t1.positive,t1.negative FROM %s AS t1, %s AS t2, %s AS t3 \
                   WHERE t2.id=t1.op1 AND t3.id=t1.op2)" tbl_real tbl_from tbl_ids_left tbl_ids_right;       
    Printf.sprintf "INSERT IGNORE INTO %s (SELECT * FROM %s) \                       
                   ON DUPLICATE KEY UPDATE %s.positive=(%s.positive || VALUES(positive)), %s.negative=(%s.negative || VALUES(negative))" 
                   tbl_tmp tbl_real tbl_tmp tbl_tmp tbl_tmp tbl_tmp;  
    Printf.sprintf "INSERT INTO %s (id,real_id) (SELECT t1.id,t2.id FROM %s AS t1,%s AS t2 WHERE (t1.id1,t1.id2)=(t2.id1,t2.id2))" 
                   tbl_ids tbl_real tbl_tmp;
    Printf.sprintf "INSERT INTO %s (SELECT * FROM %s)" tbl_dest tbl_tmp;
    Printf.sprintf "TRUNCATE TABLE %s" tbl_tmp;  
    Printf.sprintf "TRUNCATE TABLE %s" tbl_real;         
    Printf.sprintf "DROP TABLE %s" tbl_from]
  
(**============ Update axioms ==============**)
let update_axioms db tbl_ids tbl_tmp tbl_dest =    
  DB.query_list db false [
    Printf.sprintf "INSERT IGNORE INTO %s (sub,sup) (SELECT t2.real_id,t3.real_id FROM %s AS t1, %s AS t2, %s AS t3 \
                    WHERE t2.id=t1.id1 AND t3.id=t1.id2)" tbl_dest tbl_tmp tbl_ids tbl_ids;
    Printf.sprintf "DROP TABLE %s" tbl_tmp]
  
(**============ Complete axioms ==============**)
(** Deals with equivalences **)
(** For each I=A inter B positive, adds I=>A & I=>B **)
let complete_axioms db =
  DB.query_list db false [         
    Printf.sprintf "INSERT INTO %s (sub,sup) (SELECT * FROM ((SELECT * FROM %s) UNION\
                                             (SELECT id,id1 FROM %s WHERE positive=1) UNION\
                                             (SELECT id,id2 FROM %s WHERE positive=1)) AS t)" 
                                             Config.tbl_ax_class_compl Config.tbl_ax_class
                                             Config.tbl_id_class_int Config.tbl_id_class_int]

(**============ loading ontology from the input channel ==============**)
let load new_filename flag =
  let continue = ref false in
  let db = Common.open_connection () in

  (** Check for status **)
  let (_,s)=Common.get_status db "load" in
  let status = ref s in
  if flag = Common.FLnone then
  begin
    if !status=3 then  (* loading done *)
      (Printf.fprintf stderr "Loading was already processed, use -f -l to force reloading\n"; flush stderr)
    else if !status>0 then  (* loading in progress *)
      (Printf.fprintf stderr "Loading is in progress, use -f -l to force reloading or just './db -r -d database_name' to resume loading.\n"; flush stderr)
    else    
      continue := true
  end
  else if flag = Common.FLforce then 
  begin
    let (_,s)=Common.get_status db "closure"  in Common.set_status db "closure"  "deprecated" s;
    let (_,s)=Common.get_status db "classify" in Common.set_status db "classify" "deprecated" s;
    let (_,s)=Common.get_status db "output "  in Common.set_status db "output"   "deprecated" s;        
    status:=0;
    continue:=true
  end
  else if flag = Common.FLresume then 
  begin
    if !status=0 then
      (Printf.fprintf stderr "No file loaded. Use './db -d database_name -l file_name' to load an ontology.\n"; flush stderr)
    else
      continue:=true;
  end;
    
  if !continue then
  begin 
    let filename = (
      if !status>0 then
        let (f,_) = Common.get_status db "filename" in f
      else
      begin
      Common.set_status db "filename" new_filename 0;
      Common.set_status db "load" "" 1;
      new_filename
      end)
    in   
    
    Printf.fprintf stderr "Loading:\n"; flush stderr;  
    let begt = Unix.gettimeofday () in
    Printf.fprintf stderr "1. Loading the ontology...   "; flush stderr;
    if !status<2 then
    begin
      drop_tables db;
      drop_tmp_tables db;
      DB.query_list db false ["SET max_heap_table_size = 50*1024*1024"];     
      create_tables db;  
       
      (** Loads ontology into database **)
      let input = open_in filename in
      ProgressBar.init (in_channel_length input);    
      let lexbuf = Lexing.from_channel input in
      let (ontologyIRI,info) =
        try
          Fowl_parser.owl_ontologyDocument Fowl_lexer.token lexbuf
        with Parsing.Parse_error ->
            let err_lexeme = Lexing.lexeme lexbuf in
            let err_pos = lexbuf.Lexing.lex_curr_p.Lexing.pos_cnum - lexbuf.Lexing.lex_curr_p.Lexing.pos_bol in
            Printf.fprintf stderr "\nLine %n, characters %n-%n:\nSyntax error: unexpected \"%s\"\n"
              lexbuf.Lexing.lex_curr_p.Lexing.pos_lnum
              (err_pos - String.length err_lexeme + 1)
              err_pos
              err_lexeme;
            raise Parsing.Parse_error
      in     
      flush_buffers info true;
      resolve_ids info;    
      Common.set_status db "ontologyIRI" ontologyIRI 0; 
      Common.set_status db "load" "" 2;    
      let endt = Unix.gettimeofday () in Printf.fprintf stderr " done in %fs" (endt -. begt); flush stderr; 
    end;
    let begt = Unix.gettimeofday () in
    Printf.fprintf stderr "\n2. Updating IDs...           "; flush stderr;    
    if !status<3 then
    begin
      ProgressBar.init 3;  
      drop_tmp_tables db;
      ProgressBar.step ();
      complete_axioms db;
      ProgressBar.step ();   
      DB.query_list db false [
        Printf.sprintf "ALTER TABLE %s DROP update_id" Config.tbl_id_class_int;
        Printf.sprintf "ALTER TABLE %s DROP update_id" Config.tbl_id_class_ext; 
        Printf.sprintf "ANALYZE TABLE %s" Config.tbl_id_class_int;
        Printf.sprintf "ANALYZE TABLE %s" Config.tbl_id_class_ext;      
        Printf.sprintf "ANALYZE TABLE %s" Config.tbl_ax_class_compl;
        Printf.sprintf "ANALYZE TABLE %s" Config.tbl_ax_object]; 
      Common.set_status db "load" "done" 3;             
      ProgressBar.step ();
      let endt = Unix.gettimeofday () in Printf.fprintf stderr " done in %fs" (endt -. begt); flush stderr;
    end;    
    Printf.fprintf stderr "\n"; flush stderr;       
  end


