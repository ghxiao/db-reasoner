type command_flag = FLforce | FLresume | FLnone

val database : string ref
val open_connection : unit -> DB_mysql.S.db_handle
val exec_script : DB_mysql.S.db_handle -> string -> DB_mysql.S.query_result

val set_status : DB_mysql.S.db_handle -> string -> string -> int -> unit
val get_status : DB_mysql.S.db_handle -> string -> (string * int)
