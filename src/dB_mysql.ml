open DB_sig

(** This file use the Mysql library (install the libmysql-ocaml-dev package) **)

let mysql_error2db_error e =
  match e with
| Mysql.Aborting_connection -> Aborting_connection
| Mysql.Access_denied_error -> Access_denied_error
| Mysql.Alter_info -> Alter_info
| Mysql.Bad_db_error -> Bad_db_error
| Mysql.Bad_field_error -> Bad_field_error
| Mysql.Bad_host_error -> Bad_host_error
| Mysql.Bad_null_error -> Bad_null_error
| Mysql.Bad_table_error -> Bad_table_error
| Mysql.Blob_cant_have_default -> Blob_cant_have_default
| Mysql.Blob_key_without_length -> Blob_key_without_length
| Mysql.Blob_used_as_key -> Blob_used_as_key
| Mysql.Blobs_and_no_terminated -> Blobs_and_no_terminated
| Mysql.Cant_create_db -> Cant_create_db
| Mysql.Cant_create_file -> Cant_create_file
| Mysql.Cant_create_table -> Cant_create_table
| Mysql.Cant_create_thread -> Cant_create_thread
| Mysql.Cant_delete_file -> Cant_delete_file
| Mysql.Cant_drop_field_or_key -> Cant_drop_field_or_key
| Mysql.Cant_find_dl_entry -> Cant_find_dl_entry 
| Mysql.Cant_find_system_rec -> Cant_find_system_rec
| Mysql.Cant_find_udf -> Cant_find_udf
| Mysql.Cant_get_stat -> Cant_get_stat
| Mysql.Cant_get_wd -> Cant_get_wd
| Mysql.Cant_initialize_udf -> Cant_initialize_udf
| Mysql.Cant_lock -> Cant_lock
| Mysql.Cant_open_file -> Cant_open_file
| Mysql.Cant_open_library -> Cant_open_library
| Mysql.Cant_read_charset -> Cant_read_charset
| Mysql.Cant_read_dir -> Cant_read_dir
| Mysql.Cant_remove_all_fields -> Cant_remove_all_fields
| Mysql.Cant_reopen_table -> Cant_reopen_table
| Mysql.Cant_set_wd -> Cant_set_wd
| Mysql.Checkread -> Checkread
| Mysql.Columnaccess_denied_error -> Columnaccess_denied_error
| Mysql.Commands_out_of_sync -> Commands_out_of_sync
| Mysql.Con_count_error -> Con_count_error
| Mysql.Conn_host_error -> Conn_host_error
| Mysql.Connection_error -> Connection_error
| Mysql.Db_create_exists -> Db_create_exists
| Mysql.Db_drop_delete -> Db_drop_delete
| Mysql.Db_drop_exists -> Db_drop_exists
| Mysql.Db_drop_rmdir -> Db_drop_rmdir
| Mysql.Dbaccess_denied_error -> Dbaccess_denied_error
| Mysql.Delayed_cant_change_lock -> Delayed_cant_change_lock
| Mysql.Delayed_insert_table_locked -> Delayed_insert_table_locked
| Mysql.Disk_full -> Disk_full
| Mysql.Dup_entry -> Dup_entry
| Mysql.Dup_fieldname -> Dup_fieldname
| Mysql.Dup_key -> Dup_key
| Mysql.Dup_keyname -> Dup_keyname 
| Mysql.Dup_unique -> Dup_unique
| Mysql.Empty_query -> Empty_query
| Mysql.Error_on_close -> Error_on_close
| Mysql.Error_on_read -> Error_on_read
| Mysql.Error_on_rename -> Error_on_rename 
| Mysql.Error_on_write -> Error_on_write
| Mysql.Field_specified_twice -> Field_specified_twice
| Mysql.File_exists_error -> File_exists_error
| Mysql.File_not_found -> File_not_found
| Mysql.File_used -> File_used 
| Mysql.Filsort_abort -> Filsort_abort
| Mysql.Forcing_close -> Forcing_close
| Mysql.Form_not_found -> Form_not_found
| Mysql.Function_not_defined -> Function_not_defined
| Mysql.Get_errno -> Get_errno
| Mysql.Got_signal -> Got_signal
| Mysql.Grant_wrong_host_or_user -> Grant_wrong_host_or_user
| Mysql.Handshake_error -> Handshake_error
| Mysql.Hashchk -> Hashchk
| Mysql.Host_is_blocked -> Host_is_blocked
| Mysql.Host_not_privileged -> Host_not_privileged
| Mysql.Illegal_grant_for_table -> Illegal_grant_for_table
| Mysql.Illegal_ha -> Illegal_ha
| Mysql.Insert_info -> Insert_info
| Mysql.Insert_table_used -> Insert_table_used
| Mysql.Invalid_default -> Invalid_default
| Mysql.Invalid_group_func_use -> Invalid_group_func_use
| Mysql.Invalid_use_of_null -> Invalid_use_of_null
| Mysql.Ipsock_error -> Ipsock_error 
| Mysql.Key_column_does_not_exits -> Key_column_does_not_exits
| Mysql.Key_not_found -> Key_not_found
| Mysql.Kill_denied_error -> Kill_denied_error
| Mysql.Load_info -> Load_info
| Mysql.Localhost_connection -> Localhost_connection
| Mysql.Mix_of_group_func_and_fields -> Mix_of_group_func_and_fields
| Mysql.Multiple_pri_key -> Multiple_pri_key
| Mysql.Namedpipe_connection -> Namedpipe_connection
| Mysql.Namedpipeopen_error -> Namedpipeopen_error
| Mysql.Namedpipesetstate_error -> Namedpipesetstate_error
| Mysql.Namedpipewait_error -> Namedpipewait_error
| Mysql.Net_error_on_write -> Net_error_on_write
| Mysql.Net_fcntl_error -> Net_fcntl_error
| Mysql.Net_packet_too_large -> Net_packet_too_large
| Mysql.Net_packets_out_of_order -> Net_packets_out_of_order 
| Mysql.Net_read_error -> Net_read_error
| Mysql.Net_read_error_from_pipe -> Net_read_error_from_pipe 
| Mysql.Net_read_interrupted -> Net_read_interrupted 
| Mysql.Net_uncompress_error -> Net_uncompress_error
| Mysql.Net_write_interrupted -> Net_write_interrupted
| Mysql.Nisamchk -> Nisamchk
| Mysql.No -> No 
| Mysql.No_db_error -> No_db_error 
| Mysql.No_raid_compiled -> No_raid_compiled
| Mysql.No_such_index -> No_such_index 
| Mysql.No_such_table -> No_such_table 
| Mysql.No_such_thread -> No_such_thread 
| Mysql.No_tables_used -> No_tables_used
| Mysql.No_unique_logfile -> No_unique_logfile
| Mysql.Non_uniq_error -> Non_uniq_error
| Mysql.Nonexisting_grant -> Nonexisting_grant
| Mysql.Nonexisting_table_grant -> Nonexisting_table_grant
| Mysql.Nonuniq_table -> Nonuniq_table
| Mysql.Normal_shutdown -> Normal_shutdown 
| Mysql.Not_allowed_command -> Not_allowed_command 
| Mysql.Not_form_file -> Not_form_file
| Mysql.Not_keyfile -> Not_keyfile
| Mysql.Null_column_in_index -> Null_column_in_index
| Mysql.Old_keyfile -> Old_keyfile
| Mysql.Open_as_readonly -> Open_as_readonly 
| Mysql.Out_of_memory -> Out_of_memory
| Mysql.Out_of_resources -> Out_of_resources
| Mysql.Out_of_sortmemory -> Out_of_sortmemory
| Mysql.Outofmemory -> Outofmemory
| Mysql.Parse_error -> Parse_error
| Mysql.Password_anonymous_user -> Password_anonymous_user
| Mysql.Password_no_match -> Password_no_match
| Mysql.Password_not_allowed -> Password_not_allowed
| Mysql.Primary_cant_have_null -> Primary_cant_have_null
| Mysql.Ready -> Ready
| Mysql.Record_file_full -> Record_file_full
| Mysql.Regexp_error -> Regexp_error
| Mysql.Requires_primary_key -> Requires_primary_key 
| Mysql.Server_gone_error -> Server_gone_error
| Mysql.Server_handshake_err -> Server_handshake_err
| Mysql.Server_lost -> Server_lost
| Mysql.Server_shutdown -> Server_shutdown
| Mysql.Shutdown_complete -> Shutdown_complete
| Mysql.Socket_create_error -> Socket_create_error
| Mysql.Stack_overrun -> Stack_overrun 
| Mysql.Syntax_error -> Syntax_error
| Mysql.Table_cant_handle_auto_increment -> Table_cant_handle_auto_increment
| Mysql.Table_cant_handle_blob -> Table_cant_handle_blob
| Mysql.Table_exists_error -> Table_exists_error
| Mysql.Table_must_have_columns -> Table_must_have_columns
| Mysql.Table_not_locked -> Table_not_locked
| Mysql.Table_not_locked_for_write -> Table_not_locked_for_write
| Mysql.Tableaccess_denied_error -> Tableaccess_denied_error
| Mysql.Tcp_connection -> Tcp_connection
| Mysql.Textfile_not_readable -> Textfile_not_readable
| Mysql.Too_big_fieldlength -> Too_big_fieldlength
| Mysql.Too_big_rowsize -> Too_big_rowsize
| Mysql.Too_big_select -> Too_big_select
| Mysql.Too_big_set -> Too_big_set 
| Mysql.Too_long_ident -> Too_long_ident 
| Mysql.Too_long_key -> Too_long_key
| Mysql.Too_long_string -> Too_long_string
| Mysql.Too_many_delayed_threads -> Too_many_delayed_threads
| Mysql.Too_many_fields -> Too_many_fields
| Mysql.Too_many_key_parts -> Too_many_key_parts
| Mysql.Too_many_keys -> Too_many_keys
| Mysql.Too_many_rows -> Too_many_rows 
| Mysql.Too_many_tables -> Too_many_tables
| Mysql.Udf_exists -> Udf_exists
| Mysql.Udf_no_paths -> Udf_no_paths
| Mysql.Unexpected_eof -> Unexpected_eof
| Mysql.Unknown_character_set -> Unknown_character_set
| Mysql.Unknown_com_error -> Unknown_com_error
| Mysql.Unknown_error -> Unknown_error
| Mysql.Unknown_host -> Unknown_host
| Mysql.Unknown_procedure -> Unknown_procedure
| Mysql.Unknown_table -> Unknown_table
| Mysql.Unsupported_extension -> Unsupported_extension
| Mysql.Update_info -> Update_info
| Mysql.Update_without_key_in_safe_mode -> Update_without_key_in_safe_mode
| Mysql.Version_error -> Version_error
| Mysql.Wrong_auto_key -> Wrong_auto_key
| Mysql.Wrong_column_name -> Wrong_column_name
| Mysql.Wrong_db_name -> Wrong_db_name
| Mysql.Wrong_field_spec -> Wrong_field_spec
| Mysql.Wrong_field_terminators -> Wrong_field_terminators
| Mysql.Wrong_field_with_group -> Wrong_field_with_group
| Mysql.Wrong_group_field -> Wrong_group_field
| Mysql.Wrong_host_info -> Wrong_host_info
| Mysql.Wrong_key_column -> Wrong_key_column 
| Mysql.Wrong_mrg_table -> Wrong_mrg_table
| Mysql.Wrong_outer_join -> Wrong_outer_join
| Mysql.Wrong_paramcount_to_procedure -> Wrong_paramcount_to_procedure 
| Mysql.Wrong_parameters_to_procedure -> Wrong_parameters_to_procedure 
| Mysql.Wrong_sub_key -> Wrong_sub_key 
| Mysql.Wrong_sum_select -> Wrong_sum_select
| Mysql.Wrong_table_name -> Wrong_table_name
| Mysql.Wrong_value_count -> Wrong_value_count
| Mysql.Wrong_value_count_on_row -> Wrong_value_count_on_row 
| Mysql.Yes   -> Yes

let raise_error dbd =
  match Mysql.status dbd with
    | Mysql.StatusError e -> raise (DBError (mysql_error2db_error e))
    | _ -> ()
    
let mysql_dbty2db_dbty t =
  match t with
    | Mysql.IntTy -> IntTy
    | Mysql.FloatTy -> FloatTy
    | Mysql.StringTy -> StringTy
    | Mysql.SetTy -> SetTy
    | Mysql.EnumTy -> EnumTy
    | Mysql.DateTimeTy -> DateTimeTy
    | Mysql.DateTy -> DateTy
    | Mysql.TimeTy -> TimeTy
    | Mysql.YearTy -> YearTy
    | Mysql.TimeStampTy -> TimeStampTy
    | Mysql.UnknownTy -> UnknownTy
    | Mysql.Int64Ty -> Int64Ty
    | Mysql.BlobTy -> BlobTy
    | Mysql.DecimalTy -> DecimalTy
      

module S =
struct

  type db_handle = Mysql.dbd
  
  let connect 
  dbhost  (*	database server host	*)
  dbport  (*	port	*)
  dbuser  (*	database user	*)
  dbpwd   (*	user password	*)
  dbname  (*	database name	*)  
  : db_handle
  =
    let db = 
      {
       	Mysql.dbhost = dbhost; 
       	Mysql.dbname = dbname; 	
       	Mysql.dbport = dbport; 	
       	Mysql.dbpwd  = dbpwd; 	
       	Mysql.dbuser = dbuser; 	
      }
    in
    try
      Mysql.connect db
    with | _ -> raise (DBError Aborting_connection)
  
  type query_result  = Mysql.result
  
  let query dbd sqlquery : query_result =
  try
    Mysql.exec dbd sqlquery
  with | _ -> raise_error dbd; Mysql.exec dbd sqlquery

  let fetch_array res = Mysql.fetch res

  let num_rows res = Mysql.size res
  
  let insert_id dbd = Mysql.insert_id dbd
  
  let escape str = Mysql.escape str
end

module DBmysql = MakeDB(S)
