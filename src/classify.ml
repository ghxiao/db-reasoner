open DB_mysql

module DB = DBmysql

(** Drops output tables **) 
let drop_tables db =
  DB.drop_table db Config.tbl_cl_class;
  DB.drop_table db Config.tbl_cl_object

(** Drops temporary output tables **) 
let drop_tmp_tables db =  
  DB.drop_table db Config_classify.tbl_tmp_axioms;
  DB.drop_table db Config_classify.tbl_tmp_part;
  DB.drop_table db Config_classify.tbl_tmp_queue;    
  DB.drop_table db Config_classify.tbl_tmp_cl_o;
  DB.drop_table db Config_classify.tbl_tmp_cl_c;
  DB.drop_table db Config_classify.tbl_tmp_ext_neg; 
  DB.drop_table db Config_classify.tbl_tmp_ext_pos;
  DB.drop_table db Config_classify.tbl_tmp_ext_pos_part;
  DB.drop_table db Config_classify.tbl_tmp_ext_cl
  
(** Creates output tables **) 
let create_tables db =
  DB.query_list db false [
    Config.tbl_cl_class_create;
    Config.tbl_cl_object_create;
    Config_classify.tbl_tmp_axioms_create;
    Config_classify.tbl_tmp_part_create;
    Config_classify.tbl_tmp_queue_create;
    Config_classify.tbl_tmp_cl_o_create;
    Config_classify.tbl_tmp_cl_c_create;
    Config_classify.tbl_tmp_ext_neg_create;
    Config_classify.tbl_tmp_ext_pos_create;
    Config_classify.tbl_tmp_ext_pos_part_create;
    Config_classify.tbl_tmp_ext_cl_create]

(** Copy a part of tbl_base into tbl_tmp: the axioms between b and b+s and those with the same predicate **) 
let select_part db tbl_base tbl_tmp s=
  DB.query_list db false [
    Printf.sprintf "TRUNCATE %s" Config_classify.tbl_tmp_part;
    Printf.sprintf "TRUNCATE %s" Config_classify.tbl_tmp_cl_o;
    Printf.sprintf "INSERT INTO %s SELECT id FROM %s ORDER BY ord ASC LIMIT %d" Config_classify.tbl_tmp_part tbl_base s;  
    Printf.sprintf "DELETE FROM %s ORDER BY ord ASC LIMIT %d" tbl_base s; 
    Printf.sprintf "INSERT INTO %s SELECT id,id,0 FROM %s" tbl_tmp Config_classify.tbl_tmp_part;
    ]

(** Copy a part of tbl_base into tbl_tmp: the s first axioms and those with the same predicate **) 
let reload_part db s =
  DB.query_list db false [ 
    Printf.sprintf "TRUNCATE %s" Config_classify.tbl_tmp_part;
    Printf.sprintf "TRUNCATE %s" Config_classify.tbl_tmp_cl_c;    
    Printf.sprintf "INSERT INTO %s (SELECT id FROM %s ORDER BY ord ASC LIMIT %d)" Config_classify.tbl_tmp_part Config_classify.tbl_tmp_queue s;
    Printf.sprintf "INSERT INTO %s SELECT t1.* FROM %s AS t1 JOIN %s AS t2 ON t1.sub=t2.id" 
                    Config_classify.tbl_tmp_cl_c Config.tbl_cl_class Config_classify.tbl_tmp_part;
    Printf.sprintf "INSERT IGNORE INTO %s SELECT id,id,1 FROM %s" Config_classify.tbl_tmp_cl_c Config_classify.tbl_tmp_part;   
    Printf.sprintf "TRUNCATE %s" Config_classify.tbl_tmp_ext_pos_part;
    Printf.sprintf "INSERT IGNORE INTO %s SELECT t1.* FROM %s AS t1 JOIN %s AS t2 ON t1.sup_c=t2.id"
                    Config_classify.tbl_tmp_ext_pos_part Config_classify.tbl_tmp_ext_pos Config_classify.tbl_tmp_part;
    Printf.sprintf "DELETE FROM %s ORDER BY ord ASC LIMIT %d" Config_classify.tbl_tmp_queue s]
                                                             
(** Compute the classification for implication **) 
let compute_imply db tbl_base tbl_tmp l =  
  DB.query_list db false [
    Printf.sprintf "INSERT IGNORE INTO %s (SELECT t1.sub,t2.sup,%d FROM %s AS t1 JOIN %s AS t2 ON t1.sup=t2.sub WHERE t1.length=%d)"
                    tbl_tmp (l+1) tbl_tmp tbl_base l];
  (DB.row_count db)<>0

(** Compute the classification for intersection **)
let compute_inter db len last_inter_len =  
  let tmp = Config_classify.tbl_tmp_cl_c in 
  let n = DB.table_count db tmp in
  DB.query_list db false [
      Printf.sprintf "INSERT IGNORE INTO %s SELECT t2.sub, t3.id,%d FROM %s AS t2 JOIN (%s AS t3, %s AS t1) 
                      ON t1.sub = t2.sub AND t1.sup = t3.id1 AND t2.sup = t3.id2 AND t2.length>%d"
                      tmp (len+1) tmp Config.tbl_id_class_int tmp last_inter_len;
      Printf.sprintf "INSERT IGNORE INTO %s SELECT t1.sub, t3.id,%d FROM %s AS t1 JOIN (%s AS t3, %s AS t2) 
                      ON t1.sub = t2.sub AND t1.sup = t3.id1 AND t2.sup = t3.id2 AND t2.length<=%d AND t1.length>%d" 
                      tmp (len+1) tmp Config.tbl_id_class_int tmp last_inter_len last_inter_len];
  ((DB.table_count db tmp)-n)<>0

(** Compute the classification for existencial restriction **) 
let compute_exist db =
   DB.query_list db false
(*  Printf.fprintf stderr "---------------------\n"; flush stderr;
  List.iter (fun q -> let begt = Unix.gettimeofday () in let _ = DB.query db q in
      let endt = Unix.gettimeofday () in Printf.fprintf stderr " done in %fs\n" (endt -. begt); flush stderr;) *)
   [Printf.sprintf "TRUNCATE %s" Config_classify.tbl_tmp_ext_cl;
    Printf.sprintf "INSERT IGNORE INTO %s SELECT t1.sub,t3.sup FROM %s AS t2 JOIN (%s AS t1, %s AS t3) ON t1.sup_c=t2.sub AND t3.sub_c=t2.sup AND t1.sup_o=t3.sub_o AND t1.sub<>t3.sup"
                    Config_classify.tbl_tmp_ext_cl Config_classify.tbl_tmp_cl_c Config_classify.tbl_tmp_ext_pos_part Config_classify.tbl_tmp_ext_neg;                    
    Printf.sprintf "DELETE FROM t1 USING %s AS t1 JOIN %s AS t2 ON t1.sub=t2.sub AND t1.sup=t2.sup" Config_classify.tbl_tmp_ext_cl Config_classify.tbl_tmp_axioms;                                         
    Printf.sprintf "DELETE FROM t1 USING %s AS t1 JOIN %s AS t2 ON t1.sub=t2.sub AND t1.sup=t2.sup" Config_classify.tbl_tmp_ext_cl Config.tbl_cl_class; 
    Printf.sprintf "UPDATE %s AS t1,(SELECT DISTINCT sub FROM %s) AS t2 SET new=1 WHERE t1.sup=t2.sub" Config.tbl_cl_class Config_classify.tbl_tmp_ext_cl;
    Printf.sprintf "INSERT IGNORE INTO %s (id) SELECT t1.sub FROM %s AS t1 JOIN (SELECT DISTINCT sub FROM %s) AS t2 ON t1.sup=t2.sub"
                    Config_classify.tbl_tmp_queue Config.tbl_cl_class Config_classify.tbl_tmp_ext_cl;         
    Printf.sprintf "INSERT IGNORE INTO %s SELECT * FROM %s" Config_classify.tbl_tmp_axioms Config_classify.tbl_tmp_ext_cl]
                                   
(** Compute the classification for objects **)
let compute_object_classify db =   
  let compute_classify_tmp () = 
    let len = ref 0 in
    while (compute_imply db Config.tbl_ax_object Config_classify.tbl_tmp_cl_o !len) do
      incr len;
    done;   
    DB.query_list db false [   
      Printf.sprintf "INSERT IGNORE INTO %s (sub,sup) (SELECT sub,sup FROM %s)" Config.tbl_cl_object Config_classify.tbl_tmp_cl_o;
      Printf.sprintf "TRUNCATE TABLE %s" Config_classify.tbl_tmp_cl_o]
  in 
  
  (* Process all axioms *)
  DB.query_list db false [
    Printf.sprintf "TRUNCATE %s" Config_classify.tbl_tmp_cl_o;
    Printf.sprintf "INSERT IGNORE INTO %s (id) SELECT id FROM %s" Config_classify.tbl_tmp_queue Config_classify.tbl_tmp_part;
    Printf.sprintf "INSERT IGNORE INTO %s (id) SELECT id FROM %s" Config_classify.tbl_tmp_queue Config.tbl_id_object_atm];
  let (_,last_i)=Common.get_status db "classify" in    
  let n = DB.table_count db Config_classify.tbl_tmp_queue in
  let i = ref last_i in
  let s = 5000 in
  let ok = ref true in  
  ProgressBar.init (max !i (!i+n));  
  while !ok do    
    select_part db Config_classify.tbl_tmp_queue Config_classify.tbl_tmp_cl_o s;
    ProgressBar.set_state !i;
    i := !i + s;
    compute_classify_tmp ();
    Common.set_status db "classify" "object" !i;
    if (DB.table_count db Config_classify.tbl_tmp_queue) = 0 then ok := false
  done;
  ProgressBar.set_state (max 1 n);
  DB.query_list db false [
    Printf.sprintf "TRUNCATE TABLE %s" Config_classify.tbl_tmp_queue;
    Printf.sprintf "ALTER TABLE %s ADD INDEX sup (sup)" Config.tbl_cl_object]
  
(** Compute the classification for classes **)  

let compute_class_classify db =   
  let compute_classify_tmp () = 
    let ok = ref true in
    let len = ref 1 in
    let last_inter = ref 0 in
    while !ok do
      while !ok do
        let imply_success = compute_imply db Config_classify.tbl_tmp_axioms Config_classify.tbl_tmp_cl_c !len in
        if imply_success then 
          incr len
        else  
          ok := false  
      done;
      let inter_success = compute_inter db !len !last_inter in
      last_inter := !len;
      if inter_success then
      begin
        ok := true;
        incr len
      end
    done;  
    DB.query_list db false [
      Printf.sprintf "UPDATE %s AS t1,%s AS t2 SET t1.new=0,t2.length=0 WHERE t2.length=1 AND t1.sub=t2.sub AND t1.sup=t2.sup" Config.tbl_cl_class Config_classify.tbl_tmp_cl_c;
      Printf.sprintf "DELETE FROM %s WHERE length=0" Config_classify.tbl_tmp_cl_c; 
      Printf.sprintf "INSERT INTO %s SELECT sub,sup,0 FROM %s" Config.tbl_cl_class Config_classify.tbl_tmp_cl_c]
  in 
  
  (* Process all axioms *)
  DB.query_list db false [
    Printf.sprintf "TRUNCATE %s" Config_classify.tbl_tmp_cl_c;  
    Printf.sprintf "INSERT IGNORE INTO %s SELECT sub,sup FROM %s" Config_classify.tbl_tmp_axioms Config.tbl_ax_class_compl;
    Printf.sprintf "INSERT IGNORE INTO %s SELECT id,id1,id2 FROM %s WHERE positive=1" Config_classify.tbl_tmp_ext_pos Config.tbl_id_class_ext;
    Printf.sprintf "INSERT IGNORE INTO %s SELECT t2.sub,t1.id2,t1.id FROM %s AS t1 JOIN %s AS t2 ON t1.id1=t2.sup AND t1.negative=1" 
                    Config_classify.tbl_tmp_ext_neg Config.tbl_id_class_ext Config.tbl_cl_object;
    Printf.sprintf "INSERT IGNORE INTO %s (id) SELECT id FROM %s"    Config_classify.tbl_tmp_queue Config_classify.tbl_tmp_part;                    
    Printf.sprintf "INSERT IGNORE INTO %s (id) SELECT id    FROM %s" Config_classify.tbl_tmp_queue Config.tbl_id_class_atm;                   
    Printf.sprintf "INSERT IGNORE INTO %s (id) SELECT sup_c FROM %s" Config_classify.tbl_tmp_queue Config_classify.tbl_tmp_ext_pos;                       
    Printf.sprintf "ANALYZE TABLE %s" Config_classify.tbl_tmp_ext_neg;
    Printf.sprintf "ANALYZE TABLE %s" Config_classify.tbl_tmp_ext_pos;                    
    Printf.sprintf "ANALYZE TABLE %s" Config_classify.tbl_tmp_axioms];

  let (_,last_i)=Common.get_status db "classify" in   
  let ok = ref true in     
  let processed = ref last_i in 
(*  let last_analyse = ref 1 in *)
  let s = 5000 in
  ProgressBar.init 100;
  while !ok do 
    let count = DB.table_count db Config_classify.tbl_tmp_queue in
    let pct = !processed * 100 / (!processed + count) in
    ProgressBar.set_state pct;
    if count<s then
      processed := !processed + count
    else
      processed := !processed + s;
    
    if count=0 then
      ok := false
    else
    begin      
      reload_part db s;      
      compute_classify_tmp ();
     (* if !processed >= 2 * !last_analyse then
      begin
        let _ = DB.query db (Printf.sprintf "ANALYZE TABLE %s" Config_classify.tbl_tmp_axioms) in
        let _ = DB.query db (Printf.sprintf "ANALYZE TABLE %s" Config.tbl_cl_class) in     
        last_analyse := !processed
      end;*)
      compute_exist db;  
    end;
    Common.set_status db "classify" "class" !processed;      
  done
   
    
let classify flag =
  let continue = ref false in
  let db = Common.open_connection () in

  (** Check for status **)
  let (s,_)=Common.get_status db "load" in
  if (String.compare s "done") != 0 then
    (Printf.fprintf stderr "The ontology is not loaded. Use './db -d database_name -l file_name' to load it.\n"; flush stderr)
  else begin

  let (s,_)=Common.get_status db "classify" in
  let status = ref s in
  if flag = Common.FLnone then
  begin
    if (String.compare !status "done") = 0 then  (* Classification done *)
      (Printf.fprintf stderr "Classification was already computed, use -f to force restarting classification.\n"; flush stderr)
    else if (String.compare !status "deprecated") = 0 then  (* Classification deprecated *)
      (Printf.fprintf stderr "Classification is deprecated, use -f to force restarting classification.\n"; flush stderr)  
    else if (String.compare !status "") <> 0 then  (* Classification in progress *)
      (Printf.fprintf stderr "Classification is in progress, use -f to force restarting classification or just './db -r -d database_name' to resume it.\n"; flush stderr)
    else    
      continue := true
  end
  else if flag = Common.FLforce then 
  begin
    let (_,s)=Common.get_status db "taxonomy" in Common.set_status db "taxonomy" "deprecated" s;
    let (_,s)=Common.get_status db "output "  in Common.set_status db "output"   "deprecated" s;        
    status := "";
    continue:=true
  end
  else if flag = Common.FLresume then 
  begin
    if (String.compare !status "deprecated") = 0 then  (* Classification deprecated *)
      (Printf.fprintf stderr "WARNING: Classification is deprecated, use -f -c to restart classification.\n"; flush stderr);
    continue:=true;
  end;
  
  if !continue then
  begin  
    Printf.fprintf stderr "Classifying:\n"; flush stderr; 
    if (String.compare !status "") = 0 then 
    begin    
      drop_tables db;
      drop_tmp_tables db;
      let _ = DB.query db "SET max_heap_table_size = 50*1024*1024" in    
      create_tables db;
      Common.set_status db "classify" "object" 0;
    end;
    
    Printf.fprintf stderr "1. Object properties...      "; flush stderr;   
    if (String.compare !status "") = 0 || (String.compare !status "object") = 0 then
    begin
      let begt = Unix.gettimeofday () in
      compute_object_classify db;
      Common.set_status db "classify" "class" 0;      
      let endt = Unix.gettimeofday () in Printf.fprintf stderr " done in %fs" (endt -. begt); flush stderr;     
    end;
        
    Printf.fprintf stderr "\n2. Classes...                "; flush stderr;   
    if (String.compare !status "") = 0 || (String.compare !status "object") = 0 || (String.compare !status "class") = 0 then
    begin    
      let begt = Unix.gettimeofday () in
      compute_class_classify db;
      Common.set_status db "classify" "analyze" 0; 
      let endt = Unix.gettimeofday () in Printf.fprintf stderr " done in %fs" (endt -. begt); flush stderr;
    end;
    Printf.fprintf stderr "\n"; flush stderr;
          
    if (String.compare !status "done") <> 0 then
    begin
      drop_tmp_tables db;   
      DB.query_list db false [
        Printf.sprintf "ANALYZE TABLE %s" Config.tbl_cl_class;
        Printf.sprintf "ANALYZE TABLE %s" Config.tbl_cl_object];
      Common.set_status db "classify" "done" 0;                   
    end
  end
  end

