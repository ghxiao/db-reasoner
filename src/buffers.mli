module type BuffSig =
  sig
    type t
    val create : DB_mysql.S.db_handle -> string -> string -> string -> int -> t
    val flush : t -> bool -> unit
    val add : t -> string -> unit
  end
module HDBuffer : BuffSig
module MEMBuffer : BuffSig
module BufferFamily :
  functor (Buffer : BuffSig) ->
    sig
      type t = {
        fdb : DB_mysql.S.db_handle;
        ftable : string;
        fcreate : string;
        fformat : string;
        foption_end : string;
        fmax : int;
        family : (int, Buffer.t) Hashtbl.t;
      }
      val create : DB_mysql.S.db_handle -> string -> string -> string -> string -> int -> int -> t
      val get_buffer : t -> int -> Buffer.t
      val add : t -> int -> string -> unit
      val flush_buffers : t -> unit
      val length : t -> int
    end
