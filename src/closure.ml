open DB_mysql

module DB = DBmysql

(** Drops output tables **) 
let drop_tables db =
  DB.drop_table db Config.tbl_tc_class;
  DB.drop_table db Config.tbl_tc_object

(** Drops temporary output tables **) 
let drop_tmp_tables db =  
  DB.drop_table db Config_closure.tbl_tmp_axioms;
  DB.drop_table db Config_closure.tbl_tmp_part;
  DB.drop_table db Config_closure.tbl_tmp_queue;    
  DB.drop_table db Config_closure.tbl_tmp_tc_o;
  DB.drop_table db Config_closure.tbl_tmp_tc_c;
  DB.drop_table db Config_closure.tbl_tmp_ext_neg; 
  DB.drop_table db Config_closure.tbl_tmp_ext_pos;
  DB.drop_table db Config_closure.tbl_tmp_ext_pos_part;
  DB.drop_table db Config_closure.tbl_tmp_ext_tc;
  DB.drop_table db Config_closure.tbl_tmp_ext_tc_by_left
  
(** Creates output tables **) 
let create_tables db =
  DB.query_list db false [
    Config.tbl_tc_class_create;
    Config.tbl_tc_object_create;
    Config_closure.tbl_tmp_axioms_create;
    Config_closure.tbl_tmp_part_create;
    Config_closure.tbl_tmp_queue_create;
    Config_closure.tbl_tmp_tc_o_create;
    Config_closure.tbl_tmp_tc_c_create;
    Config_closure.tbl_tmp_ext_neg_create;
    Config_closure.tbl_tmp_ext_pos_create;
    Config_closure.tbl_tmp_ext_pos_part_create;
    Config_closure.tbl_tmp_ext_tc_create;
    Config_closure.tbl_tmp_ext_tc_by_left_create]

(** Copy a part of tbl_base into tbl_tmp: the axioms between b and b+s and those with the same predicate **) 
let select_part db tbl_base tbl_tmp s=
  DB.query_list db false [
    Printf.sprintf "TRUNCATE %s" Config_closure.tbl_tmp_part;
    Printf.sprintf "INSERT INTO %s SELECT id FROM %s ORDER BY ord ASC LIMIT %d" Config_closure.tbl_tmp_part tbl_base s;  
    Printf.sprintf "DELETE FROM %s ORDER BY ord ASC LIMIT %d" tbl_base s; 
    Printf.sprintf "INSERT INTO %s SELECT id,id,0 FROM %s" tbl_tmp Config_closure.tbl_tmp_part;
    ]

(** Copy a part of tbl_base into tbl_tmp: the s first axioms and those with the same predicate **) 
let reload_part db s =
  DB.query_list db false [ 
    Printf.sprintf "TRUNCATE %s" Config_closure.tbl_tmp_part;
    Printf.sprintf "INSERT INTO %s (SELECT id FROM %s ORDER BY ord ASC LIMIT %d)" Config_closure.tbl_tmp_part Config_closure.tbl_tmp_queue s;
    Printf.sprintf "INSERT INTO %s SELECT t1.* FROM %s AS t1 JOIN %s AS t2 ON t1.sub=t2.id" 
                    Config_closure.tbl_tmp_tc_c Config.tbl_tc_class Config_closure.tbl_tmp_part;
    Printf.sprintf "INSERT IGNORE INTO %s SELECT id,id,1 FROM %s" Config_closure.tbl_tmp_tc_c Config_closure.tbl_tmp_part;   
    Printf.sprintf "TRUNCATE %s" Config_closure.tbl_tmp_ext_pos_part;
    Printf.sprintf "INSERT IGNORE INTO %s SELECT t1.* FROM %s AS t1 JOIN %s AS t2 ON t1.sup_c=t2.id"
                    Config_closure.tbl_tmp_ext_pos_part Config_closure.tbl_tmp_ext_pos Config_closure.tbl_tmp_part;
    Printf.sprintf "DELETE FROM %s ORDER BY ord ASC LIMIT %d" Config_closure.tbl_tmp_queue s]
                                                             
(** Compute the closure for implication **) 
let compute_imply db tbl_base tbl_tmp l =  
  DB.query_list db false [
    Printf.sprintf "INSERT IGNORE INTO %s (SELECT t1.sub,t2.sup,%d FROM %s AS t1 JOIN %s AS t2 ON t1.sup=t2.sub WHERE t1.length=%d)"
                    tbl_tmp (l+1) tbl_tmp tbl_base l];
  (DB.row_count db)<>0

(** Compute the closure for intersection **)
let compute_inter db len last_inter_len =  
  let tmp = Config_closure.tbl_tmp_tc_c in 
  let n = DB.table_count db tmp in
  DB.query_list db false [
      Printf.sprintf "INSERT IGNORE INTO %s SELECT t2.sub, t3.id,%d FROM %s AS t2 JOIN (%s AS t3, %s AS t1) 
                      ON t1.sub = t2.sub AND t1.sup = t3.id1 AND t2.sup = t3.id2 AND t2.length>%d"
                      tmp (len+1) tmp Config.tbl_id_class_int tmp last_inter_len;
      Printf.sprintf "INSERT IGNORE INTO %s SELECT t1.sub, t3.id,%d FROM %s AS t1 JOIN (%s AS t3, %s AS t2) 
                      ON t1.sub = t2.sub AND t1.sup = t3.id1 AND t2.sup = t3.id2 AND t2.length<=%d AND t1.length>%d" 
                      tmp (len+1) tmp Config.tbl_id_class_int tmp last_inter_len last_inter_len];
  ((DB.table_count db tmp)-n)<>0

(** Compute the closure for existencial restriction **) 
let compute_exist db =
  DB.query_list db false [  
    Printf.sprintf "INSERT IGNORE INTO %s SELECT t1.sub,t3.sup FROM %s AS t2 JOIN (%s AS t1, %s AS t3) ON t1.sup_c=t2.sub AND t3.sub_c=t2.sup AND t3.sub_o=t1.sup_o AND t1.sub<>t3.sup"
                    Config_closure.tbl_tmp_ext_tc Config_closure.tbl_tmp_tc_c Config_closure.tbl_tmp_ext_pos_part Config_closure.tbl_tmp_ext_neg;
    Printf.sprintf "TRUNCATE %s" Config_closure.tbl_tmp_tc_c;                     
    Printf.sprintf "DELETE FROM t1 USING %s AS t1 JOIN %s AS t2 ON t1.sub=t2.sub AND t1.sup=t2.sup" Config_closure.tbl_tmp_ext_tc Config_closure.tbl_tmp_axioms;                                         
    Printf.sprintf "DELETE FROM t1 USING %s AS t1 JOIN %s AS t2 ON t1.sub=t2.sub AND t1.sup=t2.sup" Config_closure.tbl_tmp_ext_tc Config.tbl_tc_class; 
    Printf.sprintf "INSERT IGNORE INTO %s SELECT t1.sub,t2.sup FROM %s AS t1 JOIN %s AS t2 ON t1.sup=t2.sub" 
                    Config_closure.tbl_tmp_ext_tc_by_left Config.tbl_tc_class Config_closure.tbl_tmp_ext_tc;  
    Printf.sprintf "DELETE FROM t1 USING %s AS t1 JOIN %s AS t2 ON t1.sub=t2.sub AND t1.sup=t2.sup" Config_closure.tbl_tmp_ext_tc_by_left Config.tbl_tc_class;
    Printf.sprintf "INSERT INTO %s SELECT *,1 FROM %s" Config.tbl_tc_class Config_closure.tbl_tmp_ext_tc_by_left;
    Printf.sprintf "INSERT IGNORE INTO %s (id) SELECT sub FROM %s" Config_closure.tbl_tmp_queue Config_closure.tbl_tmp_ext_tc_by_left;    
    Printf.sprintf "INSERT IGNORE INTO %s SELECT * FROM %s" Config_closure.tbl_tmp_axioms Config_closure.tbl_tmp_ext_tc;
    Printf.sprintf "TRUNCATE %s" Config_closure.tbl_tmp_ext_tc;
    Printf.sprintf "TRUNCATE %s" Config_closure.tbl_tmp_ext_tc_by_left]
                                   
(** Compute the closure for objects **)
let compute_object_closure db =   
  let compute_closure_tmp () = 
    let len = ref 0 in
    while (compute_imply db Config.tbl_ax_object Config_closure.tbl_tmp_tc_o !len) do
      incr len;
    done;   
    DB.query_list db false [   
      Printf.sprintf "INSERT IGNORE INTO %s (sub,sup) (SELECT sub,sup FROM %s)" Config.tbl_tc_object Config_closure.tbl_tmp_tc_o;
      Printf.sprintf "TRUNCATE TABLE %s" Config_closure.tbl_tmp_tc_o]
  in 
  
  (* Process all axioms *)
  DB.query_list db false [
    Printf.sprintf "INSERT IGNORE INTO %s (id) SELECT id FROM %s" Config_closure.tbl_tmp_queue Config_closure.tbl_tmp_part;
    Printf.sprintf "INSERT IGNORE INTO %s (id) SELECT id FROM %s" Config_closure.tbl_tmp_queue Config.tbl_id_object_atm];
  let (_,last_i)=Common.get_status "closure" in    
  let n = DB.table_count db Config_closure.tbl_tmp_queue in
  let i = ref last_i in
  let s = 10000 in
  let ok = ref true in  
  ProgressBar.init (max !i (!i+n));  
  while !ok do    
    select_part db Config_closure.tbl_tmp_queue Config_closure.tbl_tmp_tc_o s;
    ProgressBar.set_state !i;
    i := !i + s;
    compute_closure_tmp ();
    Common.get_status "closure" "object" !i;
    if (DB.table_count db Config_closure.tbl_tmp_queue) = 0 then ok := false
  done;
  ProgressBar.set_state (max 1 n);
  DB.query_list db false [Printf.sprintf "TRUNCATE TABLE %s" Config_closure.tbl_tmp_queue]
  
(** Compute the closure for classes **)  

let compute_class_closure db =   
  let compute_closure_tmp () = 
    let ok = ref true in
    let len = ref 1 in
    let last_inter = ref 0 in
    while !ok do
      while !ok do
        let imply_success = compute_imply db Config_closure.tbl_tmp_axioms Config_closure.tbl_tmp_tc_c !len in
        if imply_success then 
          incr len
        else  
          ok := false  
      done;
      let inter_success = compute_inter db !len !last_inter in
      last_inter := !len;
      if inter_success then
      begin
        ok := true;
        incr len
      end
    done;  
    DB.query_list db false [
      Printf.sprintf "UPDATE %s AS t1,%s AS t2 SET t1.new=0,t2.length=0 WHERE t2.length=1 AND t1.sub=t2.sub AND t1.sup=t2.sup" Config.tbl_tc_class Config_closure.tbl_tmp_tc_c;
      Printf.sprintf "DELETE FROM %s WHERE length=0" Config_closure.tbl_tmp_tc_c; 
      Printf.sprintf "INSERT INTO %s SELECT sub,sup,0 FROM %s" Config.tbl_tc_class Config_closure.tbl_tmp_tc_c]
  in 
  
  (* Process all axioms *)
  DB.query_list db false [  
    Printf.sprintf "ALTER TABLE %s ADD INDEX sup (sup)" Config.tbl_tc_object;  
    Printf.sprintf "INSERT IGNORE INTO %s SELECT sub,sup FROM %s" Config_closure.tbl_tmp_axioms Config.tbl_ax_class_compl;
    Printf.sprintf "INSERT IGNORE INTO %s SELECT id,id1,id2 FROM %s WHERE positive=1" Config_closure.tbl_tmp_ext_pos Config.tbl_id_class_ext;
    Printf.sprintf "INSERT IGNORE INTO %s SELECT t2.sub,t1.id2,t1.id FROM %s AS t1 JOIN %s AS t2 ON t1.id1=t2.sup AND t1.negative=1" 
                    Config_closure.tbl_tmp_ext_neg Config.tbl_id_class_ext Config.tbl_tc_object;
    Printf.sprintf "INSERT IGNORE INTO %s (id) SELECT id FROM %s"    Config_closure.tbl_tmp_queue Config_closure.tbl_tmp_part;                    
    Printf.sprintf "INSERT IGNORE INTO %s (id) SELECT id    FROM %s" Config_closure.tbl_tmp_queue Config.tbl_id_class_atm;                   
    Printf.sprintf "INSERT IGNORE INTO %s (id) SELECT sup_c FROM %s" Config_closure.tbl_tmp_queue Config_closure.tbl_tmp_ext_pos;                       
    Printf.sprintf "ANALYZE TABLE %s" Config_closure.tbl_tmp_ext_neg;
    Printf.sprintf "ANALYZE TABLE %s" Config_closure.tbl_tmp_ext_pos;                    
    Printf.sprintf "ANALYZE TABLE %s" Config_closure.tbl_tmp_axioms];
    
  let (_,last_i)=Common.get_status "closure" in   
  let ok = ref true in     
  let processed = ref last_i in
  let last_analyse = ref 1 in
  let s = 10000 in
  ProgressBar.init 100;
  while !ok do 
    let count = DB.table_count db Config_closure.tbl_tmp_queue in
    ProgressBar.set_state (!processed * 100 / (!processed + count));
    if count<s then
      processed := !processed + count
    else
      processed := !processed + s;
    
    if count=0 then
      ok := false
    else
    begin      
      reload_part db s;      
      compute_closure_tmp ();
      if !processed >= 2 * !last_analyse then
      begin
       (* let _ = DB.query db (Printf.sprintf "ANALYZE TABLE %s" Config_closure.tbl_tmp_axioms) in
        let _ = DB.query db (Printf.sprintf "ANALYZE TABLE %s" Config.tbl_tc_class) in      *)
        last_analyse := !processed
      end;
      compute_exist db;  
    end;
    Common.get_status "closure" "class" !processed;      
  done
   
    
let closure flag =
  let continue = ref false in
  let db = Common.open_connection () in

  (** Check for status **)
  let (s,_)=Common.get_status "load" in
  if (String.compare s "done") != 0 then
    Printf.fprintf stderr "The ontology is not loaded. Use './db -d database_name -l file_name' to load it.\n"
  else begin

  let (s,_)=Common.get_status "closure" in
  let status = ref s in
  if flag = Common.FLnone then
  begin
    if (String.compare !status "done") = 0 then  (* closure done *)
      Printf.fprintf stderr "Closure is done, use -f to restart closure.\n"
    else if (String.compare !status "deprecated") = 0 then  (* closure deprecated *)
      Printf.fprintf stderr "Closure is deprecated, use -f to restart closure or just './db -r -d database_name' to resume it.\n";   
    else if (String.compare !status "") <> 0 then  (* closure in progress *)
      Printf.fprintf stderr "Closure is in progress, use -f to restart closure or just './db -r -d database_name' to resume it.\n";
    else    
      continue := true
  end
  else if flag = Common.FLforce then 
  begin
    let (_,s)=Common.get_status "classify" in Common.set_status "classify" "deprecated" s
    let (_,s)=Common.get_status "output "  in Common.set_status "output"   "deprecated" s        
    status := "";
    continue:=true
  end
  else if flag = Common.FLresume then (continue:=true);

  if !continue then
  begin  
    Printf.fprintf stderr "Computing the transitive closure:\n"; flush stderr; 
    if (String.compare !status "") = 0 then 
    begin    
      drop_tables db;
      drop_tmp_tables db;
      let _ = DB.query db "SET max_heap_table_size = 50*1024*1024" in    
      create_tables db;
      Common.set_status "closure" "object" 0;
    end;
    
    Printf.fprintf stderr "1. Object closure...         "; flush stderr;   
    if (String.compare !status "object") = 0 then
    begin
      let begt = Unix.gettimeofday () in
      compute_object_closure db;
      Common.set_status "closure" "class" 0;      
      let endt = Unix.gettimeofday () in Printf.fprintf stderr " done in %fs\n" (endt -. begt); flush stderr;     
    end
        
    Printf.fprintf stderr "2. Class closure...          "; flush stderr;   
    if (String.compare !status "class") = 0 then
    begin    
      let begt = Unix.gettimeofday () in        
      compute_class_closure db;
      Common.set_status "closure" "analyze" 0; 
      let endt = Unix.gettimeofday () in Printf.fprintf stderr " done in %fs\n" (endt -. begt); flush stderr;
    end
    
    drop_tmp_tables db;   
    DB.query_list db false [
      Printf.sprintf "ANALYZE TABLE %s" Config.tbl_tc_class;
      Printf.sprintf "ANALYZE TABLE %s" Config.tbl_tc_object];
    Common.set_status "closure" "done" 0;                   
  end
  end

