type dbty =
| 	IntTy
| 	FloatTy
| 	StringTy
| 	SetTy
| 	EnumTy
| 	DateTimeTy
| 	DateTy
| 	TimeTy
| 	YearTy
| 	TimeStampTy
| 	UnknownTy
| 	Int64Ty
| 	BlobTy
| 	DecimalTy


type error_code =
| 	Aborting_connection
| 	Access_denied_error
| 	Alter_info
| 	Bad_db_error
| 	Bad_field_error
| 	Bad_host_error
| 	Bad_null_error
| 	Bad_table_error
| 	Blob_cant_have_default
| 	Blob_key_without_length
| 	Blob_used_as_key
| 	Blobs_and_no_terminated
| 	Cant_create_db
| 	Cant_create_file
| 	Cant_create_table
| 	Cant_create_thread
| 	Cant_delete_file
| 	Cant_drop_field_or_key
| 	Cant_find_dl_entry
| 	Cant_find_system_rec
| 	Cant_find_udf
| 	Cant_get_stat
| 	Cant_get_wd
| 	Cant_initialize_udf
| 	Cant_lock
| 	Cant_open_file
| 	Cant_open_library
| 	Cant_read_charset
| 	Cant_read_dir
| 	Cant_remove_all_fields
| 	Cant_reopen_table
| 	Cant_set_wd
| 	Checkread
| 	Columnaccess_denied_error
| 	Commands_out_of_sync
| 	Con_count_error
| 	Conn_host_error
| 	Connection_error
| 	Db_create_exists
| 	Db_drop_delete
| 	Db_drop_exists
| 	Db_drop_rmdir
| 	Dbaccess_denied_error
| 	Delayed_cant_change_lock
| 	Delayed_insert_table_locked
| 	Disk_full
| 	Dup_entry
| 	Dup_fieldname
| 	Dup_key
| 	Dup_keyname
| 	Dup_unique
| 	Empty_query
| 	Error_on_close
| 	Error_on_read
| 	Error_on_rename
| 	Error_on_write
| 	Field_specified_twice
| 	File_exists_error
| 	File_not_found
| 	File_used
| 	Filsort_abort
| 	Forcing_close
| 	Form_not_found
| 	Function_not_defined
| 	Get_errno
| 	Got_signal
| 	Grant_wrong_host_or_user
| 	Handshake_error
| 	Hashchk
| 	Host_is_blocked
| 	Host_not_privileged
| 	Illegal_grant_for_table
| 	Illegal_ha
| 	Insert_info
| 	Insert_table_used
| 	Invalid_default
| 	Invalid_group_func_use
| 	Invalid_use_of_null
| 	Ipsock_error
| 	Key_column_does_not_exits
| 	Key_not_found
| 	Kill_denied_error
| 	Load_info
| 	Localhost_connection
| 	Mix_of_group_func_and_fields
| 	Multiple_pri_key
| 	Namedpipe_connection
| 	Namedpipeopen_error
| 	Namedpipesetstate_error
| 	Namedpipewait_error
| 	Net_error_on_write
| 	Net_fcntl_error
| 	Net_packet_too_large
| 	Net_packets_out_of_order
| 	Net_read_error
| 	Net_read_error_from_pipe
| 	Net_read_interrupted
| 	Net_uncompress_error
| 	Net_write_interrupted
| 	Nisamchk
| 	No
| 	No_db_error
| 	No_raid_compiled
| 	No_such_index
| 	No_such_table
| 	No_such_thread
| 	No_tables_used
| 	No_unique_logfile
| 	Non_uniq_error
| 	Nonexisting_grant
| 	Nonexisting_table_grant
| 	Nonuniq_table
| 	Normal_shutdown
| 	Not_allowed_command
| 	Not_form_file
| 	Not_keyfile
| 	Null_column_in_index
| 	Old_keyfile
| 	Open_as_readonly
| 	Out_of_memory
| 	Out_of_resources
| 	Out_of_sortmemory
| 	Outofmemory
| 	Parse_error
| 	Password_anonymous_user
| 	Password_no_match
| 	Password_not_allowed
| 	Primary_cant_have_null
| 	Ready
| 	Record_file_full
| 	Regexp_error
| 	Requires_primary_key
| 	Server_gone_error
| 	Server_handshake_err
| 	Server_lost
| 	Server_shutdown
| 	Shutdown_complete
| 	Socket_create_error
| 	Stack_overrun
| 	Syntax_error
| 	Table_cant_handle_auto_increment
| 	Table_cant_handle_blob
| 	Table_exists_error
| 	Table_must_have_columns
| 	Table_not_locked
| 	Table_not_locked_for_write
| 	Tableaccess_denied_error
| 	Tcp_connection
| 	Textfile_not_readable
| 	Too_big_fieldlength
| 	Too_big_rowsize
| 	Too_big_select
| 	Too_big_set
| 	Too_long_ident
| 	Too_long_key
| 	Too_long_string
| 	Too_many_delayed_threads
| 	Too_many_fields
| 	Too_many_key_parts
| 	Too_many_keys
| 	Too_many_rows
| 	Too_many_tables
| 	Udf_exists
| 	Udf_no_paths
| 	Unexpected_eof
| 	Unknown_character_set
| 	Unknown_com_error
| 	Unknown_error
| 	Unknown_host
| 	Unknown_procedure
| 	Unknown_table
| 	Unsupported_extension
| 	Update_info
| 	Update_without_key_in_safe_mode
| 	Version_error
| 	Wrong_auto_key
| 	Wrong_column_name
| 	Wrong_db_name
| 	Wrong_field_spec
| 	Wrong_field_terminators
| 	Wrong_field_with_group
| 	Wrong_group_field
| 	Wrong_host_info
| 	Wrong_key_column
| 	Wrong_mrg_table
| 	Wrong_outer_join
| 	Wrong_paramcount_to_procedure
| 	Wrong_parameters_to_procedure
| 	Wrong_sub_key
| 	Wrong_sum_select
| 	Wrong_table_name
| 	Wrong_value_count
| 	Wrong_value_count_on_row
| 	Yes

exception DBError of error_code

module type DBSig = 
sig
  (** Connection handle **)
  type db_handle

  (** Connects to database: (host address) (port) (user) (password) (database name), use None for default **)
  val connect : (string option) -> (int option) -> (string option) -> (string option) -> (string option) -> db_handle
  
  (** Query result **)
  type query_result
  
  (** Perform an SQL request **)
  val query : db_handle -> string -> query_result
  
  (** Give the next row of a result, None if empty or NULL **)
  val fetch_array : query_result -> string option array option
  
  (** Number of rows of a query **)
  val num_rows : query_result -> int64

  (** Return last inserted id **)
  val insert_id : db_handle -> int64
  
  (** Escape a string **)
  val escape : string -> string
end
    
module MakeDB(DB : DBSig) = 
struct
  type db_handle = DB.db_handle
  let connect = DB.connect
  type query_result = DB.query_result
  let query dbd str = (*Printf.fprintf stderr "%s\n" str; flush stderr;*)
    try
      DB.query dbd str
    with | DBError Parse_error -> Printf.fprintf stderr "Exception: You have an error in your SQL syntaxe:\n%s\n" str; raise (DBError Parse_error)
         | e -> Printf.fprintf stderr "Exception: Here is the SQL query:\n%s\n" str; raise e          
  let fetch_array = DB.fetch_array
  let num_rows = DB.num_rows
  let insert_id = DB.insert_id  
  let escape = DB.escape

  let query_list dbd progress l = if progress then ProgressBar.init (List.length l); List.iter (fun q -> let _ = query dbd q in if progress then ProgressBar.step()) l  
  
  let table_exist dbd table =
    let sqlquery = Printf.sprintf "SHOW TABLES LIKE \"%s\"" (escape table) in
      num_rows (query dbd sqlquery) > Int64.zero
  
  let drop_table dbd table =
    let sqlquery = Printf.sprintf "DROP TABLE IF EXISTS %s" (escape table) in let _ = query dbd sqlquery in ()
  
  let fetch_int res =
  match fetch_array res with 
    |Some a -> 
    begin 
      match a.(0) with 
        |Some str -> int_of_string str 
        |_ -> 0
    end 
    |_ -> 0
    
  let row_count dbd = fetch_int (query dbd "SELECT ROW_COUNT()")
  
  let table_count dbd table = fetch_int (query dbd (Printf.sprintf "SELECT COUNT(*) FROM %s" table))
end
