(** Temporary tables for loading **)
(* Non merged axioms for classes *)
let tbl_tmp_c_ax         = "tmp_c_ax"
let tbl_tmp_c_ax_create  = Printf.sprintf "CREATE TABLE %s (op1 INT NOT NULL,\
                                                            op2 INT NOT NULL,\
                                                            id1 INT,\
                                                            id2 INT,\
                                                            INDEX (op1),\
                                                            INDEX (op2)\
                                                           ) ENGINE=MEMORY" tbl_tmp_c_ax    

(* Non merged axioms for objects *)
let tbl_tmp_o_ax        = "tmp_o_ax"
let tbl_tmp_o_ax_create = Printf.sprintf "CREATE TABLE %s (op1 INT NOT NULL,\
                                                           op2 INT NOT NULL,\
                                                           id1 INT,\
                                                           id2 INT,\
                                                           INDEX (op1),\
                                                           INDEX (op2)\
                                                          ) ENGINE=MEMORY" tbl_tmp_o_ax

(* Non merged equivalences for classes *)
let tbl_tmp_c_eq         = "tmp_c_eq"
let tbl_tmp_c_eq_create  = Printf.sprintf "CREATE TABLE %s (repr INT NOT NULL,\
                                                            op INT NOT NULL,\
                                                            repr_id INT,\
                                                            id INT,\
                                                            INDEX (repr),\
                                                            INDEX (op)\
                                                           ) ENGINE=MEMORY" tbl_tmp_c_eq

(* Non merged equivalences for objects *)
let tbl_tmp_o_eq         = "tmp_o_eq"
let tbl_tmp_o_eq_create  = Printf.sprintf "CREATE TABLE %s (repr INT NOT NULL,\
                                                            op INT NOT NULL,\
                                                            repr_id INT,\                                                            
                                                            id INT,\
                                                            INDEX (repr),\
                                                            INDEX (op)\
                                                           ) ENGINE=MEMORY" tbl_tmp_o_eq

(* Non merged atomic objects *)
let tbl_tmp_o_atm         = "tmp_o_atm"
let tbl_tmp_o_atm_create  = Printf.sprintf "CREATE TABLE %s (id INT NOT NULL,\
                                                             name VARCHAR(10) NOT NULL\
                                                            ) ENGINE=MEMORY" tbl_tmp_o_atm 
                                                            
(* Non merged atomic classes *)
let tbl_tmp_c_atm         = "tmp_c_atm"
let tbl_tmp_c_atm_create  = Printf.sprintf "CREATE TABLE %s (id INT NOT NULL,\
                                                             name VARCHAR(10) NOT NULL\
                                                            ) ENGINE=MEMORY" tbl_tmp_c_atm

                                                                                                                                                                     
                                                                                                          
(* Non merged intersection *)
let tbl_tmp_c_int        = "tmp_c_int"
let tbl_tmp_c_int_create = Printf.sprintf "CREATE TABLE %s (id INT NOT NULL,\
                                                            op1 INT NOT NULL,\
                                                            op2 INT NOT NULL,\
                                                            positive BOOL NOT NULL,\
                                                            negative BOOL NOT NULL,\
                                                            depth INT NOT NULL,\
                                                            id1 INT,\
                                                            id2 INT,\
                                                            INDEX (op1),\
                                                            INDEX (op2),\
                                                            INDEX (depth)\
                                                           ) ENGINE=MEMORY" tbl_tmp_c_int                                                                                                               

(* Non merged existential restriction *)
let tbl_tmp_c_ext        = "tmp_c_ext"
let tbl_tmp_c_ext_create = Printf.sprintf "CREATE TABLE %s (id INT NOT NULL,\
                                                            op1 INT NOT NULL,\                                               
                                                            op2 INT NOT NULL,\  
                                                            positive BOOL NOT NULL,\
                                                            negative BOOL NOT NULL,\                                                                                                              
                                                            depth INT NOT NULL,\
                                                            id1 INT,\
                                                            id2 INT,\                                                                                                                     
                                                            INDEX (op1),\
                                                            INDEX (op2),\
                                                            INDEX (depth)\                                                            
                                                           ) ENGINE=MEMORY" tbl_tmp_c_ext                                                     

let tbl_tmp_new_eq        = "tmp_new_eq"
let tbl_tmp_new_eq_create = Printf.sprintf "CREATE TABLE %s (repr INT NOT NULL,\
                                                             id INT NOT NULL,\
                                                             INDEX (repr)\
                                                            )" tbl_tmp_new_eq     
                                                                                                                            
(* Correspondance between real ids and assigned ids for classes *)
let tbl_tmp_ids        = "tmp_ids"
let tbl_tmp_ids_create = Printf.sprintf "CREATE TABLE %s (id INT NOT NULL,\
                                                          real_id INT NOT NULL,\
                                                          INDEX (id)\
                                                         ) ENGINE=MEMORY" tbl_tmp_ids
                                                                                                       
                                         
