open DB_mysql
module DB = DBmysql

(** Signature for the Buffer module **)
module type BuffSig = sig
  (** Buffer type **)
  type t
  
  (** Buffer creation, needs:                             
        a database handle,                                
        the name of the table to fill,                    
        the name of the columns to fill separated by ',', 
        additional command at the end of the request
        the size of the buffer (nb of entries)            
  **)
  
  val create : DB_mysql.S.db_handle -> string -> string -> string -> int -> t
  (** Empties the buffer into the table specified during the creation **)
  
  val flush : t -> bool -> unit
  
  (** Add an entrie to the buffer. If the buffer is full, then it is flushed **)
  val add : t -> string -> unit
end

(** Buffer using hard disk **)
module HDBuffer : BuffSig = struct
  type t = 
  {
    db : DB.db_handle;
    file : string;
    table : string;
    format : string;
    option_end : string;
    oc : out_channel ref;
    max : int;
    nbr : int ref;
  }

  (* Path to store the buffer *)
  let path = "/var/lib/mysql/reasoner/"
  
  (* Creates the buffer *)
  let create db table format option_end max =
    let file = path ^ table ^ ".mine.csv" in
    let oc = open_out file in
    let maxi = if max > 0 then max else 128 in
    {
      db = db;
      file = file;
      table = table;
      format = format;
      option_end = option_end;
      oc = ref oc;
      max = maxi;
      nbr = ref 0;
    }

  (* Flushes the buffer into database *)
  let flush buff _ =
    if !(buff.nbr) <> 0 then
    begin
      close_out !(buff.oc); 
      let query = Printf.sprintf "LOAD DATA INFILE '%s' IGNORE INTO TABLE %s FIELDS TERMINATED BY ',' ENCLOSED BY '\"' ESCAPED BY '\\\\' LINES STARTING BY '' TERMINATED BY '\\n' (%s)"
                   buff.file buff.table buff.format
      in let _ = DB.query buff.db query in
      buff.nbr := 0;
      buff.oc := open_out buff.file
    end

  (* Adds an entry to the buffer *)    
  let add buff str = 
    if !(buff.nbr) = buff.max then
      flush buff false;
    output_string !(buff.oc) (str ^ "\n");
    incr buff.nbr;
end

(** Buffer using memory **)
module MEMBuffer : BuffSig = struct
  type t = 
  {
    db : DB.db_handle;
    table : string;
    format : string;
    option_end : string;
    query : string ref;
    max : int;
    max_len : int ref;
    cur_ptr : int ref;
    nbr : int ref; 
  }

  let init_str table format = Printf.sprintf "INSERT DELAYED IGNORE INTO %s %s VALUES " table (if String.compare format "" = 0 then "" else "("^format^")")
  
  (* Creates the buffer *)
  let create db table format option_end max =
    let maxi = if max > 0 then max else 128 in
    let init = init_str table format in
    let len = String.length init in
    let max_len = 2*maxi*(String.length format)+len in
    let str = String.create max_len in
    (*let query = Printf.sprintf "LOCK TABLES %s WRITE" table in
    let _ = DB.query db query in*)
    String.blit init 0 str 0 len;
    {
      db = db;
      table = table;
      format = format;
      option_end = option_end;
      query = ref str;
      max = maxi;
      max_len = ref max_len;
      cur_ptr = ref len;
      nbr = ref 0;
    }
    
  (* Copies the character c into the buffer *)
  let add_char buff c =
    if !(buff.cur_ptr) = !(buff.max_len) then
    begin
      buff.query := !(buff.query) ^ !(buff.query);
      buff.max_len := !(buff.max_len) * 2;
    end;
    (!(buff.query)).[!(buff.cur_ptr)] <- c;
    incr buff.cur_ptr
    
  (* Copies the string str into the buffer *)
  let add_str buff str =
    let len = String.length str in
    if !(buff.cur_ptr)+len >= !(buff.max_len) then
    begin
      buff.query := !(buff.query) ^ !(buff.query);
      buff.max_len := !(buff.max_len) * 2;
    end;
    String.blit str 0 !(buff.query) !(buff.cur_ptr) len;
    buff.cur_ptr := !(buff.cur_ptr) + len
    
  (* Flushes the buffer into database *)
  let flush buff final =
    if !(buff.nbr) <> 0 then
    begin
      add_str buff buff.option_end;
      let _ = DB.query buff.db (String.sub !(buff.query) 0 !(buff.cur_ptr)) in
      let init = init_str buff.table buff.format in
      let len = String.length init in
        String.blit init 0 !(buff.query) 0 len;
        buff.cur_ptr := len;
        buff.nbr := 0
    end;
    if final then
    begin
      (*let _ = DB.query buff.db "UNLOCK TABLES" in ()*) 
      let init = init_str buff.table buff.format in
      buff.query := init;
      buff.max_len := String.length init
    end
    
  (* Adds an entry to the buffer *)        
  let add buff str = 
    if !(buff.nbr) = buff.max then
      flush buff false;
    if !(buff.nbr) > 0 then
      add_char buff ',';
    add_char buff '(';
    add_str buff str;  
    add_char buff ')';
    incr buff.nbr;
end

(** A set of indexed buffers **)
module BufferFamily (Buffer : BuffSig) = struct
type t = 
{
  fdb : DB.db_handle;
  ftable : string;
  fcreate : string;
  fformat : string;
  foption_end : string;
  fmax : int;
  family : (int,Buffer.t) Hashtbl.t;
}

(** Creates the family, needs:
  - db: the database handle
  - table: the table prefix name (buffer i will then empty into table_i)
  - create: the fields for creating the tables
  - format: the columns the buffers will fill
  - option_end : option_end statement to add at the end of the query
  - max: the maximum size of the buffers
  - fsize: the estimated number of buffers
 **)
let create db table create format option_end max fsize=
  {
    fdb = db;
    ftable = table;
    fcreate = create;
    fformat = format;
    foption_end = option_end;
    fmax = max;
    family = Hashtbl.create fsize;
  }

(** get_buffer f i returns the ith buffer of the family f **) 
let get_buffer f i =
  try
    Hashtbl.find f.family i
  with | _ ->
    let table = Printf.sprintf "%s_%d" f.ftable i in
    DB.drop_table f.fdb table;
    let query = Printf.sprintf "CREATE TABLE %s %s" table f.fcreate
    in
    let _ = DB.query f.fdb query in 
    let buff = Buffer.create f.fdb table f.fformat f.foption_end f.fmax in
      Hashtbl.add f.family i buff;
      buff

(** Add an entrie to the ith buffer. If the buffer is full, then it is flushed **)
let add f i str =
  let buff = get_buffer f i in
  Buffer.add buff str       

(** Empties all buffers of the family **)
let flush_buffers f =
  Hashtbl.iter (fun _ buff -> Buffer.flush buff true) f.family;
  Hashtbl.clear f.family

(** Number of buffers in the family **)
let length f = Hashtbl.length f.family
end
