open DB_mysql
module DB = DBmysql

(** Drops output tables **) 
let drop_tables db =
  DB.drop_table db Config.tbl_tx_imply_o;
  DB.drop_table db Config.tbl_tx_equiv_o;
  DB.drop_table db Config.tbl_tx_imply_c;
  DB.drop_table db Config.tbl_tx_equiv_c

(** Drops temporary output tables **) 
let drop_tmp_tables db =  
  DB.drop_table db Config_taxonomy.tbl_tmp_tx_atm_only;
  DB.drop_table db Config_taxonomy.tbl_tmp_tx_queue;    
  DB.drop_table db Config_taxonomy.tbl_tmp_tx_mem  
  
(** Creates output tables **) 
let create_tables db =
  DB.query_list db false [
    Config.tbl_tx_imply_o_create;
    Config.tbl_tx_equiv_o_create;
    Config.tbl_tx_imply_c_create;
    Config.tbl_tx_equiv_c_create;
    Config_taxonomy.tbl_tmp_tx_atm_only_create;
    Config_taxonomy.tbl_tmp_tx_queue_create;
    Config_taxonomy.tbl_tmp_tx_mem_create]
    
(** Classify subsumption from 'source', with name set 'atm' into 'dest_imply' and 'dest_equiv' **)    
let class_func db source atm dest_imply dest_equiv =
  Printf.fprintf stderr "1. Filtering names...        "; flush stderr;     
  let begt = Unix.gettimeofday () in
  let tmp = Config_taxonomy.tbl_tmp_tx_atm_only in   
  let (status,_) = Common.get_status db "taxonomy" in  
  if (String.compare status "iterate") <> 0 then  
  begin
    DB.query_list db true [
      Printf.sprintf "INSERT IGNORE INTO %s SELECT t1.sub,t1.sup FROM %s AS t1 JOIN (SELECT id FROM %s GROUP BY id) AS t2 ON t1.sup=t2.id WHERE t1.sub!=t1.sup" tmp source atm;
      Printf.sprintf "ALTER TABLE %s ADD INDEX (sub,sup)" tmp;   
      Printf.sprintf "INSERT INTO %s SELECT sub FROM %s GROUP BY sub" Config_taxonomy.tbl_tmp_tx_queue tmp;    
      Printf.sprintf "DELETE FROM t1 USING %s AS t1 JOIN %s AS t2 ON t1.id=t2.id" Config_taxonomy.tbl_tmp_tx_queue atm;     
      Printf.sprintf "DELETE FROM t1 USING %s AS t1 JOIN %s AS t2 ON t1.sub=t2.id" tmp Config_taxonomy.tbl_tmp_tx_queue;
      Printf.sprintf "TRUNCATE %s" Config_taxonomy.tbl_tmp_tx_queue;    
      Printf.sprintf "ALTER TABLE %s ADD INDEX (sup)" tmp; 
      Printf.sprintf "INSERT INTO %s SELECT t1.sub,t1.sup FROM %s AS t1 JOIN %s AS t2 ON t1.sub=t2.sup AND t1.sup=t2.sub AND t1.sub<t1.sup"
                      dest_equiv tmp tmp;     
      Printf.sprintf "DELETE FROM t1 USING %s AS t1 JOIN %s AS t2 ON t1.repr=t2.id" dest_equiv dest_equiv;  
      Printf.sprintf "DELETE FROM t1 USING %s AS t1 JOIN %s AS t2 ON t1.sup=t2.id" tmp dest_equiv; 
      Printf.sprintf "DELETE FROM t1 USING %s AS t1 JOIN %s AS t2 ON t1.sub=t2.id" tmp dest_equiv; 
      Printf.sprintf "INSERT INTO %s SELECT repr,repr FROM %s GROUP BY repr" dest_equiv dest_equiv;       
      Printf.sprintf "INSERT IGNORE INTO %s SELECT t1.id,t1.id FROM %s AS t1 JOIN %s AS t2 ON t1.id=t2.id AND t1.name!=t2.name" dest_equiv atm atm;   
      Printf.sprintf "ANALYZE TABLE %s" tmp; 
      Printf.sprintf "INSERT IGNORE INTO %s SELECT sub FROM %s GROUP BY sub" Config_taxonomy.tbl_tmp_tx_queue tmp];
    let endt = Unix.gettimeofday () in Printf.fprintf stderr " done in %fs" (endt -. begt); flush stderr;
  end;    
  Common.set_status db "taxonomy" "iterate" 0;     
  let begt = Unix.gettimeofday () in
  Printf.fprintf stderr "\n2. Computing taxonomy...     "; flush stderr;
  let i = ref 0 in
  let step = 100 in
  let max = DB.table_count db Config_taxonomy.tbl_tmp_tx_queue in
  ProgressBar.init max;
  while !i<max do
    ProgressBar.set_state !i;
    DB.query_list db false [    
      Printf.sprintf "INSERT IGNORE INTO %s SELECT t1.sub,t1.sup,1 FROM %s AS t1 JOIN (SELECT * FROM %s ORDER BY id ASC LIMIT %d) AS t2 ON t1.sub=t2.id" 
                      Config_taxonomy.tbl_tmp_tx_mem tmp Config_taxonomy.tbl_tmp_tx_queue step;
      Printf.sprintf "UPDATE %s AS t1,%s AS t2,%s AS t3 SET t3.direct=0 WHERE t3.sub=t1.sub AND t1.sup=t2.sub AND t2.sup=t3.sup" Config_taxonomy.tbl_tmp_tx_mem tmp Config_taxonomy.tbl_tmp_tx_mem; 
      Printf.sprintf "INSERT IGNORE INTO %s SELECT sub,sup FROM %s WHERE direct=1" dest_imply Config_taxonomy.tbl_tmp_tx_mem;         
      Printf.sprintf "DELETE FROM %s ORDER BY id ASC LIMIT %d" Config_taxonomy.tbl_tmp_tx_queue step;
      Printf.sprintf "TRUNCATE %s" Config_taxonomy.tbl_tmp_tx_mem];
    i := !i + step
  done;
  ProgressBar.set_state max;
  DB.query_list db false [Printf.sprintf "TRUNCATE %s" tmp]; 
  let endt = Unix.gettimeofday () in Printf.fprintf stderr " done in %fs" (endt -. begt); flush stderr  
      
(** Classify the ontology **)    
let taxonomy flag =
  let continue = ref false in
  let db = Common.open_connection () in

  (** Check for status **)
  let (s,_)=Common.get_status db "classify" in
  if (String.compare s "done") != 0 && (String.compare s "deprecated") != 0 then
    (Printf.fprintf stderr "The ontology has not been classified. Use './db -d database_name -c' to classify it.\n"; flush stderr)
  else begin

  if (String.compare s "deprecated") = 0 then
    (Printf.fprintf stderr "WARNING: Ontology's classification is deprecated. Use './db -d database_name -c' to classify it again.\n"; flush stderr);

  let (s,_)=Common.get_status db "taxonomy" in
  let status = ref s in
  if flag = Common.FLnone then
  begin
    if (String.compare !status "done") = 0 then  (* Taxonomy done *)
      (Printf.fprintf stderr "Taxonomy was already computed, use -f to force recomputing taxonomy.\n"; flush stderr)
    else if (String.compare !status "deprecated") = 0 then  (* Taxonomy deprecated *)
      (Printf.fprintf stderr "Taxonomy is deprecated, use -f to force recomputing taxonomy.\n"; flush stderr)  
    else if (String.compare !status "") <> 0 then  (* Taxonomy in progress *)
      (Printf.fprintf stderr "Taxonomy is in progress, use -f to force recomputing taxonomy or just './db -r -d database_name' to resume it.\n"; flush stderr)
    else    
      continue := true
  end
  else if flag = Common.FLforce then 
  begin
    let (_,s)=Common.get_status db "output "  in Common.set_status db "output" "deprecated" s;      
    status := "";
    Common.set_status db "taxonomy" "" 0;     
    continue:=true
  end
  else if flag = Common.FLresume then 
  begin
    if (String.compare !status "deprecated") = 0 then  (* Taxonomy deprecated *)
      (Printf.fprintf stderr "WARNING: Taxonomy is deprecated, use -f -t to recompute taxonomy.\n"; flush stderr);  
    continue:=true
  end;
  
  if !continue then
  begin
    Printf.fprintf stderr "Computing the taxonomy:\n"; flush stderr;     
    
    if (String.compare !status "") = 0 then 
    begin    
      drop_tables db;
      drop_tmp_tables db; 
      create_tables db;
    end;
    
    if (String.compare !status "analyze") <> 0 then    
      class_func db Config.tbl_cl_class Config.tbl_id_class_atm Config.tbl_tx_imply_c Config.tbl_tx_equiv_c; 
    Printf.fprintf stderr "\n"; flush stderr;
    
    if (String.compare !status "done") <> 0 then    
    begin
      Common.set_status db "taxonomy" "analyze" 0;     
      drop_tmp_tables db;
      DB.query_list db false [
        Printf.sprintf "ANALYZE TABLE %s" Config.tbl_tx_imply_o;
        Printf.sprintf "ANALYZE TABLE %s" Config.tbl_tx_equiv_o;
        Printf.sprintf "ANALYZE TABLE %s" Config.tbl_tx_imply_c;      
        Printf.sprintf "ANALYZE TABLE %s" Config.tbl_tx_equiv_c];
      Common.set_status db "taxonomy" "done" 0; 
    end        
  end
  end
