module DB = DB_mysql.DBmysql
module Buffer = Buffers.MEMBuffer
module BuffFamily = Buffers.BufferFamily(Buffer)

type concept = 
  | ATOMIC_C of string
  | INTER_RESTR of concept list
  | EXIST_RESTR of role * concept
  | NONE_C
and role =
  | ATOMIC_R of string
  | NONE_R

type axiom =
  | CONC_INCL of concept * concept
  | CONC_EQUI of concept list
  | ROLE_INCL of role * role
  | ROLE_EQUI of role list

type polarity = NEG | POS | BOTH

(**============ Information to keep in memory while loading & initialization ==============**)
type load_info =
{
  db : DB.db_handle;
  mutable max_c_atm_len : int;
  mutable max_r_atm_len : int;   
  tmp_c_ax  : Buffer.t;
  tmp_o_ax  : Buffer.t;  
  tmp_c_eq  : Buffer.t;
  tmp_o_eq  : Buffer.t;
  tmp_c_atm : Buffer.t;
  tmp_o_atm : Buffer.t;   
  tmp_c_int : Buffer.t;
  tmp_c_ext : Buffer.t;  
  id : Int64.t ref;
  count_ax : int ref;
}
                                        
let init_load () = 
  let dbd = Common.open_connection () in
  {
    db = dbd;   
    max_c_atm_len = 10;
    max_r_atm_len = 10;  
    tmp_c_ax  = Buffer.create dbd Config_load.tbl_tmp_c_ax  "op1,op2" "" 512;
    tmp_o_ax  = Buffer.create dbd Config_load.tbl_tmp_o_ax  "op1,op2" "" 512;
    tmp_c_eq  = Buffer.create dbd Config_load.tbl_tmp_c_eq  "repr,op" "" 512;
    tmp_o_eq  = Buffer.create dbd Config_load.tbl_tmp_o_eq  "repr,op" "" 512;    
    tmp_c_atm = Buffer.create dbd Config_load.tbl_tmp_c_atm "" "" 512;
    tmp_o_atm = Buffer.create dbd Config_load.tbl_tmp_o_atm "" "" 512;
    tmp_c_int = Buffer.create dbd Config_load.tbl_tmp_c_int "id,op1,op2,positive,negative,depth" "" 512;
    tmp_c_ext = Buffer.create dbd Config_load.tbl_tmp_c_ext "id,op1,op2,positive,negative,depth" "" 512; 
    id = ref Int64.zero;
    count_ax = ref 0;
  }

(**============ Common definitions ==============**)
(* Empty all buffers *)
let flush_buffers info final = 
  Buffer.flush info.tmp_c_ax  final;
  Buffer.flush info.tmp_o_ax  final;
  Buffer.flush info.tmp_c_eq  final;
  Buffer.flush info.tmp_o_eq  final;
  Buffer.flush info.tmp_c_atm final;
  Buffer.flush info.tmp_o_atm final;
  Buffer.flush info.tmp_c_int final;
  Buffer.flush info.tmp_c_ext final 
  
(** Default return value. Needed only for type checking **)
let default_c = (Int64.zero,0,"")
let default_r = (Int64.zero,"")

let is_pos polarity = (polarity = POS) || (polarity = BOTH)
let is_neg polarity = (polarity = NEG) || (polarity = BOTH)

let next_id info = info.id := Int64.succ !(info.id); !(info.id)

let rec add_list m n =
  match m,n with
    | a::u,b::v -> (a+.b)::(add_list u v)
    | _,[] -> m
    | [],_ -> n

(** Links arbitrary ids with real ones **)
let resolve_ids info =
  let resolve_ids_objects () =
    DB.query_list info.db false [ 
      Printf.sprintf "UPDATE %s AS t1, %s AS t2 SET t1.id1=t2.real_id WHERE t1.op1=t2.id" Config_load.tbl_tmp_c_ext Config_load.tbl_tmp_ids;
      Printf.sprintf "UPDATE %s AS t1, %s AS t2 SET t1.id1=t2.real_id WHERE t1.op1=t2.id" Config_load.tbl_tmp_o_ax  Config_load.tbl_tmp_ids;
      Printf.sprintf "UPDATE %s AS t1, %s AS t2 SET t1.id2=t2.real_id WHERE t1.op2=t2.id" Config_load.tbl_tmp_o_ax  Config_load.tbl_tmp_ids;  
      Printf.sprintf "UPDATE %s AS t1, %s AS t2 SET t1.id =t2.real_id WHERE t1.op =t2.id" Config_load.tbl_tmp_o_eq  Config_load.tbl_tmp_ids;
      Printf.sprintf "UPDATE %s AS t1, %s AS t2 SET t1.repr_id=t2.real_id WHERE t1.repr=t2.id" Config_load.tbl_tmp_o_eq  Config_load.tbl_tmp_ids;      
      Printf.sprintf "TRUNCATE %s" Config_load.tbl_tmp_ids]
  and resolve_ids_classes () =
    DB.query_list info.db false [ 
      Printf.sprintf "UPDATE %s AS t1, %s AS t2 SET t1.id1=t2.real_id WHERE t1.op1=t2.id" Config_load.tbl_tmp_c_int Config_load.tbl_tmp_ids;
      Printf.sprintf "UPDATE %s AS t1, %s AS t2 SET t1.id2=t2.real_id WHERE t1.op2=t2.id" Config_load.tbl_tmp_c_int Config_load.tbl_tmp_ids;  
      Printf.sprintf "UPDATE %s AS t1, %s AS t2 SET t1.id2=t2.real_id WHERE t1.op2=t2.id" Config_load.tbl_tmp_c_ext Config_load.tbl_tmp_ids;
      Printf.sprintf "UPDATE %s AS t1, %s AS t2 SET t1.id1=t2.real_id WHERE t1.op1=t2.id" Config_load.tbl_tmp_c_ax  Config_load.tbl_tmp_ids;
      Printf.sprintf "UPDATE %s AS t1, %s AS t2 SET t1.id2=t2.real_id WHERE t1.op2=t2.id" Config_load.tbl_tmp_c_ax  Config_load.tbl_tmp_ids;   
      Printf.sprintf "UPDATE %s AS t1, %s AS t2 SET t1.id =t2.real_id WHERE t1.op =t2.id" Config_load.tbl_tmp_c_eq  Config_load.tbl_tmp_ids;
      Printf.sprintf "UPDATE %s AS t1, %s AS t2 SET t1.repr_id=t2.real_id WHERE t1.repr=t2.id" Config_load.tbl_tmp_c_eq  Config_load.tbl_tmp_ids;
      Printf.sprintf "TRUNCATE %s" Config_load.tbl_tmp_ids]
  and merge_equiv_classes table_equiv =
    let ok = ref true in
    while !ok do
      DB.query_list info.db false [
        Printf.sprintf "UPDATE %s AS t3, (SELECT t2.repr_id AS mult,t1.repr_id AS merge FROM \
                                         (SELECT COUNT(*) AS n,MIN(repr_id) AS repr_id,id FROM %s GROUP BY id) AS t1 JOIN %s AS t2 ON t1.n>1 AND t1.id=t2.id) AS t4 \
                                         SET t3.repr_id=t4.merge WHERE t3.repr_id=t4.mult" table_equiv table_equiv table_equiv];
      if (DB.row_count info.db) <= 0 then ok := false;
    done
  in
 
  (* Objects *)  
  DB.query_list info.db false [
    Printf.sprintf "INSERT IGNORE INTO %s (SELECT MIN(id),name FROM %s GROUP BY name)" Config.tbl_id_object_atm Config_load.tbl_tmp_o_atm;     
    Printf.sprintf "INSERT INTO %s (SELECT t1.id AS id,t2.id AS real_id FROM %s AS t1 INNER JOIN %s AS t2 ON t1.name=t2.name)" 
                    Config_load.tbl_tmp_ids Config_load.tbl_tmp_o_atm Config.tbl_id_object_atm;                
    Printf.sprintf "TRUNCATE %s" Config_load.tbl_tmp_o_atm];
  resolve_ids_objects ();
  (* Classes *)
  DB.query_list info.db false [         
    Printf.sprintf "INSERT IGNORE INTO %s (SELECT MIN(id),name FROM %s GROUP BY name)" Config.tbl_id_class_atm Config_load.tbl_tmp_c_atm;      
    Printf.sprintf "INSERT INTO %s (SELECT t1.id AS id,t2.id AS real_id FROM %s AS t1 INNER JOIN %s AS t2 ON t1.name=t2.name)" 
                    Config_load.tbl_tmp_ids Config_load.tbl_tmp_c_atm Config.tbl_id_class_atm;
    Printf.sprintf "TRUNCATE %s" Config_load.tbl_tmp_c_atm];
  resolve_ids_classes ();  

  (* let l1 = List.map (fun query ->  let begt = Unix.gettimeofday () in let _ = DB.query info.db query in let endt = Unix.gettimeofday () in endt-.begt) commands in *)
  let res = DB.query info.db "SELECT MAX(depth) FROM tmp_c_int" in
  let maxd_inter = match DB.fetch_array res with | Some a -> begin match a.(0) with | Some str -> int_of_string str |_ -> 0 end |_-> 0 in
  let res = DB.query info.db "SELECT MAX(depth) FROM tmp_c_ext" in
  let maxd_exist = match DB.fetch_array res with | Some a -> begin match a.(0) with | Some str -> int_of_string str |_ -> 0 end |_-> 0 in
  (*let l2 = ref [] in*)
  for i = 1 to max maxd_inter maxd_exist do
    if i<=maxd_inter then
    begin
     DB.query_list info.db false [
       Printf.sprintf "INSERT IGNORE INTO %s (SELECT id,id1,id2,positive,negative,NULL FROM %s WHERE depth = %d)
                       ON DUPLICATE KEY UPDATE %s.positive=(%s.positive || VALUES(positive)), %s.negative=(%s.negative || VALUES(negative))" 
                       Config.tbl_id_class_int Config_load.tbl_tmp_c_int i Config.tbl_id_class_int Config.tbl_id_class_int Config.tbl_id_class_int Config.tbl_id_class_int;
       Printf.sprintf "INSERT INTO %s (SELECT t1.id AS id,t2.id AS real_id FROM %s AS t1 INNER JOIN %s AS t2 ON t1.id1=t2.id1 AND t1.id2=t2.id2 WHERE t1.depth=%d)" 
                       Config_load.tbl_tmp_ids Config_load.tbl_tmp_c_int Config.tbl_id_class_int i];
       resolve_ids_classes ();                         
(*     let l = List.map (fun query ->  let begt = Unix.gettimeofday () in let _ = DB.query info.db query in let endt = Unix.gettimeofday () in endt-.begt) commands in
       l2 := add_list l !l2*)
    end;
    if i<=maxd_exist then
    begin
     DB.query_list info.db false [
       Printf.sprintf "INSERT IGNORE INTO %s (SELECT id,id1,id2,positive,negative,NULL FROM %s WHERE depth = %d)
                       ON DUPLICATE KEY UPDATE %s.positive=(%s.positive || VALUES(positive)), %s.negative=(%s.negative || VALUES(negative))" 
                       Config.tbl_id_class_ext Config_load.tbl_tmp_c_ext i Config.tbl_id_class_ext Config.tbl_id_class_ext Config.tbl_id_class_ext Config.tbl_id_class_ext;
       Printf.sprintf "INSERT INTO %s (SELECT t1.id AS id,t2.id AS real_id FROM %s AS t1 INNER JOIN %s AS t2 ON t1.id1=t2.id1 AND t1.id2=t2.id2 WHERE t1.depth=%d)" 
                       Config_load.tbl_tmp_ids Config_load.tbl_tmp_c_ext Config.tbl_id_class_ext i];
       resolve_ids_classes ();                      
    (* let l = List.map (fun query ->  let begt = Unix.gettimeofday () in let _ = DB.query info.db query in let endt = Unix.gettimeofday () in endt-.begt) commands in
       l2 := add_list l !l2    *)
    end;     
  done;
  DB.query_list info.db false [
    Printf.sprintf "TRUNCATE %s" Config_load.tbl_tmp_c_int;  
    Printf.sprintf "TRUNCATE %s" Config_load.tbl_tmp_c_ext;
    Printf.sprintf "INSERT IGNORE INTO %s (sub,sup) (SELECT id1,id2 FROM %s)" Config.tbl_ax_object Config_load.tbl_tmp_o_ax;
    Printf.sprintf "INSERT IGNORE INTO %s (sub,sup) (SELECT id1,id2 FROM %s)" Config.tbl_ax_class  Config_load.tbl_tmp_c_ax;
    Printf.sprintf "ALTER TABLE %s ADD INDEX id (id)" Config_load.tbl_tmp_o_eq;  
    Printf.sprintf "ALTER TABLE %s ADD INDEX id (id)" Config_load.tbl_tmp_c_eq];
  merge_equiv_classes Config_load.tbl_tmp_o_eq;  
  DB.query_list info.db false [
    Printf.sprintf "UPDATE %s AS t1, %s AS t2 SET t1.id =t2.repr_id WHERE t1.id =t2.id"        Config.tbl_id_object_atm Config_load.tbl_tmp_o_eq;
    Printf.sprintf "UPDATE IGNORE %s AS t1, %s AS t2 SET t1.sub=t2.repr_id WHERE t1.sub=t2.id" Config.tbl_ax_object     Config_load.tbl_tmp_o_eq;
    Printf.sprintf "UPDATE IGNORE %s AS t1, %s AS t2 SET t1.sup=t2.repr_id WHERE t1.sup=t2.id" Config.tbl_ax_object     Config_load.tbl_tmp_o_eq];  
  let ok = ref true in
  while !ok do
    merge_equiv_classes Config_load.tbl_tmp_c_eq; 
    DB.query_list info.db false [    
      Printf.sprintf "UPDATE %s AS t1, %s AS t2 SET t1.id =t2.repr_id WHERE t1.id =t2.id"        Config.tbl_id_class_atm  Config_load.tbl_tmp_c_eq;          
      Printf.sprintf "UPDATE IGNORE %s AS t1, %s AS t2 SET t1.sub=t2.repr_id WHERE t1.sub=t2.id" Config.tbl_ax_class      Config_load.tbl_tmp_c_eq;
      Printf.sprintf "UPDATE IGNORE %s AS t1, %s AS t2 SET t1.sup=t2.repr_id WHERE t1.sup=t2.id" Config.tbl_ax_class      Config_load.tbl_tmp_c_eq;    
       
      (* Updating ids should be done before updating id1 and id2 because of triggers *)    
      Printf.sprintf "UPDATE %s AS t1, %s AS t2 SET t1.id=t2.repr_id WHERE t1.id =t2.id" Config.tbl_id_class_ext Config_load.tbl_tmp_c_eq;
      Printf.sprintf "UPDATE %s AS t1, %s AS t2 SET t1.id=t2.repr_id WHERE t1.id =t2.id" Config.tbl_id_class_int Config_load.tbl_tmp_c_eq;    
      Printf.sprintf "UPDATE %s AS t1, %s AS t2 SET t1.update_id=t2.repr_id WHERE t1.id1=t2.id" Config.tbl_id_class_ext Config_load.tbl_tmp_o_eq;        
      Printf.sprintf "UPDATE %s AS t1, %s AS t2 SET t1.update_id=t2.repr_id WHERE t1.id1=t2.id" Config.tbl_id_class_int Config_load.tbl_tmp_c_eq;
      (* Updates id1, in case of duplicate key, update_id is not set to NULL and we add new equivalences *)
      Printf.sprintf "UPDATE IGNORE %s SET id1=update_id,update_id=NULL WHERE update_id IS NOT NULL" Config.tbl_id_class_ext;      
      Printf.sprintf "UPDATE IGNORE %s SET id1=update_id,update_id=NULL WHERE update_id IS NOT NULL" Config.tbl_id_class_int;
      Printf.sprintf "INSERT IGNORE INTO %s SELECT t2.id,t1.id FROM %s AS t1 JOIN %s AS t2 ON t1.update_id IS NOT NULL AND t2.id1=t1.update_id AND t2.id2=t1.id2" 
                      Config_load.tbl_tmp_new_eq Config.tbl_id_class_ext Config.tbl_id_class_ext; 
      Printf.sprintf "INSERT IGNORE INTO %s SELECT t2.id,t1.id FROM %s AS t1 JOIN %s AS t2 ON t1.update_id IS NOT NULL AND t2.id1=t1.update_id AND t2.id2=t1.id2" 
                      Config_load.tbl_tmp_new_eq Config.tbl_id_class_int Config.tbl_id_class_int;
      Printf.sprintf "DELETE FROM %s WHERE update_id IS NOT NULL" Config.tbl_id_class_ext;        
      Printf.sprintf "DELETE FROM %s WHERE update_id IS NOT NULL" Config.tbl_id_class_int;     

      Printf.sprintf "UPDATE %s AS t1, %s AS t2 SET t1.update_id=t2.repr_id WHERE t1.id2=t2.id" Config.tbl_id_class_ext Config_load.tbl_tmp_c_eq;        
      Printf.sprintf "UPDATE %s AS t1, %s AS t2 SET t1.update_id=t2.repr_id WHERE t1.id2=t2.id" Config.tbl_id_class_int Config_load.tbl_tmp_c_eq;
      (* Updates id1, in case of duplicate key, update_id is not set to NULL and we add new equivalences *)                  
      Printf.sprintf "UPDATE IGNORE %s SET id2=update_id,update_id=NULL WHERE update_id IS NOT NULL" Config.tbl_id_class_ext;        
      Printf.sprintf "UPDATE IGNORE %s SET id2=update_id,update_id=NULL WHERE update_id IS NOT NULL" Config.tbl_id_class_int;
      Printf.sprintf "INSERT IGNORE INTO %s SELECT t2.id,t1.id FROM %s AS t1 JOIN %s AS t2 ON t1.update_id IS NOT NULL AND t2.id1=t1.id1 AND t2.id2=t1.update_id" 
                      Config_load.tbl_tmp_new_eq Config.tbl_id_class_ext Config.tbl_id_class_ext; 
      Printf.sprintf "INSERT IGNORE INTO %s SELECT t2.id,t1.id FROM %s AS t1 JOIN %s AS t2 ON t1.update_id IS NOT NULL AND t2.id1=t1.id1 AND t2.id2=t1.update_id" 
                      Config_load.tbl_tmp_new_eq Config.tbl_id_class_int Config.tbl_id_class_int;
      Printf.sprintf "DELETE FROM %s WHERE update_id IS NOT NULL" Config.tbl_id_class_ext;        
      Printf.sprintf "DELETE FROM %s WHERE update_id IS NOT NULL" Config.tbl_id_class_int];
    if (DB.table_count info.db Config_load.tbl_tmp_new_eq) = 0 then
      ok := false
    else
      DB.query_list info.db false [              
        Printf.sprintf "UPDATE %s AS t1, %s AS t2 SET t1.positive=1,t1.negative=1 WHERE t1.id=t2.repr" Config.tbl_id_class_ext Config_load.tbl_tmp_new_eq;
        Printf.sprintf "UPDATE %s AS t1, %s AS t2 SET t1.positive=1,t1.negative=1 WHERE t1.id=t2.repr" Config.tbl_id_class_int Config_load.tbl_tmp_new_eq;        
        Printf.sprintf "TRUNCATE %s" Config_load.tbl_tmp_c_eq;
        Printf.sprintf "INSERT INTO %s (repr_id,id) SELECT DISTINCT repr,id FROM %s" Config_load.tbl_tmp_c_eq Config_load.tbl_tmp_new_eq;
        Printf.sprintf "TRUNCATE %s" Config_load.tbl_tmp_new_eq;        
        ]
  done; 
  DB.query_list info.db false [
    Printf.sprintf "ALTER TABLE %s DROP INDEX id" Config_load.tbl_tmp_o_eq;  
    Printf.sprintf "ALTER TABLE %s DROP INDEX id" Config_load.tbl_tmp_c_eq;
    Printf.sprintf "TRUNCATE %s" Config_load.tbl_tmp_o_ax;  
    Printf.sprintf "TRUNCATE %s" Config_load.tbl_tmp_c_ax;
    Printf.sprintf "TRUNCATE %s" Config_load.tbl_tmp_o_eq;  
    Printf.sprintf "TRUNCATE %s" Config_load.tbl_tmp_c_eq]
    
 (* let l3 = List.map (fun query ->  let begt = Unix.gettimeofday () in let _ = DB.query info.db query in let endt = Unix.gettimeofday () in endt-.begt) commands in
  (l1,!l2,l3)*)
    
(**============ Returns (id,string) of an atomic object and add it to the table ==============**)
let get_id_atomic_r info str =
  let len = String.length str in
  if len > info.max_r_atm_len then
  begin
    let _ = DB.query info.db "SET max_heap_table_size = 100*1024*1024" in      
    let query1 = Printf.sprintf "ALTER TABLE %s CHANGE name name VARCHAR(%d)" Config_load.tbl_tmp_o_atm len in
    let query2 = Printf.sprintf "ALTER TABLE %s CHANGE name name VARCHAR(%d)" Config.tbl_id_object_atm len in
    let _ = DB.query info.db query1 in          
    let _ = DB.query info.db query2 in
    info.max_r_atm_len <- len    
  end;
  let id = next_id info in    
    Buffer.add info.tmp_o_atm (Printf.sprintf "%Ld,%S" id str);
    (id,str)
  
(**============ Returns (id,depth,string) of an atomic class and add it to the table ==============**)
let get_id_atomic_c info str =
  let len = String.length str in
  if len > info.max_c_atm_len then
  begin
    let _ = DB.query info.db "SET max_heap_table_size = 100*1024*1024" in     
    let query1 = Printf.sprintf "ALTER TABLE %s CHANGE name name VARCHAR(%d)" Config_load.tbl_tmp_c_atm len in
    let query2 = Printf.sprintf "ALTER TABLE %s CHANGE name name VARCHAR(%d)" Config.tbl_id_class_atm  len in
    let _ = DB.query info.db query1 in        
    let _ = DB.query info.db query2 in
    info.max_c_atm_len <- len    
  end;    
  let id = next_id info in      
    Buffer.add info.tmp_c_atm (Printf.sprintf "%Ld,%S" id str);
    (id,0,str)

(**============ Returns (id,depth,string) of a class intersection and add it to the table ==============**)
let get_id_inter_c info polarity (id1,d1,s1) (id2,d2,s2) = 
  let id = next_id info in
  let d = (max d1 d2) + 1 in 
    Buffer.add info.tmp_c_int (Printf.sprintf "%Ld,%Ld,%Ld,%d,%d,%d" id id1 id2 (if is_pos polarity then 1 else 0) (if is_neg polarity then 1 else 0) d);
    (id,d,"I("^s1^","^s2^")") 
      
(**============ Returns (id,depth,string) of an existential restriction and add it to the table ==============**)
let get_id_exist_c info polarity (id1,s1) (id2,d2,s2) =
  let id = next_id info in
  let d = d2 + 1 in 
    Buffer.add info.tmp_c_ext (Printf.sprintf "%Ld,%Ld,%Ld,%d,%d,%d" id id1 id2 (if is_pos polarity then 1 else 0) (if is_neg polarity then 1 else 0) d);
    (id,d,"E("^s1^","^s2^")") 

(**============ Get (id,depth,string) of a concept ==============**)
let rec get_class_id info polarity c = 
  match c with
    | ATOMIC_C str -> get_id_atomic_c info str
    | INTER_RESTR l ->
      (* Makes sure that intersection is commutative *)
      let ops = ref [] in
      (* Extract recursivly all operands which are intersections *)
      let rec extract_inters concepts =
        match concepts with
          | [] -> ()
          | c :: t -> 
          begin
            match c with
              | INTER_RESTR d -> extract_inters d; extract_inters t
              | _ -> ops := c :: !ops; extract_inters t
          end
      in
      extract_inters l;
      (* Sort operands *)
      let ids = List.fast_sort 
                     (fun (_,d1,s1) (_,d2,s2) -> let r = compare d1 d2 in if r = 0 then String.compare s1 s2 else r)
                     (List.map (get_class_id info polarity) !ops)
      in
      (* Add them *)
      let rec f idl = 
        match idl with
          | c1 :: c2 :: [] -> get_id_inter_c info polarity c1 c2
          | c1 :: l -> let c2 = f l in get_id_inter_c info polarity c1 c2
          | _ -> default_c
       in f ids     
    | EXIST_RESTR (r,c) -> let nr = get_object_id info r in
                           let nc = get_class_id info polarity c in 
                           get_id_exist_c info polarity nr nc
    | _ -> default_c
    
(**============ Get (id,string) of a role ==============**)
and get_object_id info r = 
  match r with
    | ATOMIC_R str -> get_id_atomic_r info str    
    | _ -> default_r


(**============ Add axiom to database ==============**)  
(*let time1 = ref []
let time2 = ref []
let time3 = ref []*)

let add_axiom info axiom = 
  if !(info.count_ax) = 40000 then 
  begin
    flush_buffers info false;
    resolve_ids info;
    (*let l1,l2,l3 = resolve_ids info in
    time1 := add_list !time1 l1;
    time2 := add_list !time2 l2;    
    time3 := add_list !time3 l3;*)
    info.count_ax := 0;
  end;
  incr info.count_ax;
  match axiom with
    | CONC_INCL (c1,c2) -> let (id1,_,_) = get_class_id info NEG c1 in
                           let (id2,_,_) = get_class_id info POS c2 in 
                           Buffer.add info.tmp_c_ax (Printf.sprintf "%Ld,%Ld" id1 id2)
    | ROLE_INCL (o1,o2) -> let (id1,_) = get_object_id info o1 in
                           let (id2,_) = get_object_id info o2 in 
                           Buffer.add info.tmp_o_ax (Printf.sprintf "%Ld,%Ld" id1 id2)                            
    | CONC_EQUI l ->
    begin
      let repr = ref Int64.zero in
      List.iter (fun c -> let (id,_,_) = get_class_id info BOTH c in
      if (Int64.compare !repr Int64.zero) = 0 then repr := id;
      Buffer.add info.tmp_c_eq (Printf.sprintf "%Ld,%Ld" !repr id)) l;
    end      
    | ROLE_EQUI l ->
    begin
      let repr = ref Int64.zero in
      List.iter (fun o -> let (id,_) = get_object_id info o in
      if (Int64.compare !repr Int64.zero) = 0 then repr := id;
      Buffer.add info.tmp_o_eq (Printf.sprintf "%Ld,%Ld" !repr id)) l;
    end   

