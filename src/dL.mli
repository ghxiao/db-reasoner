type concept =
    ATOMIC_C of string
  | INTER_RESTR of concept list
  | EXIST_RESTR of role * concept
  | NONE_C
and role = ATOMIC_R of string | NONE_R

type axiom =
    CONC_INCL of concept * concept
  | CONC_EQUI of concept list
  | ROLE_INCL of role * role
  | ROLE_EQUI of role list
  
type load_info 
val init_load : unit -> load_info
val flush_buffers : load_info -> bool -> unit

val resolve_ids : load_info -> unit
val add_axiom : load_info -> axiom -> unit
