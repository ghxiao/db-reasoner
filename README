DB is an experimental reasoner for ontologies based
on the reasoner CB (http://code.google.com/p/cb-reasoner/).
DB is able to deal with very large ontologies while 
using a database for computation.

Currently DB supports a fragment of OWL which corresponds
to the DL EL. 

I. Installation

In order to compile the program from sources, you will need:
 * ocaml v.3.10 
     (http://caml.inria.fr/index.en.html)
 * the ocaml-mysql librairie v.1.0
     (http://raevnos.pennmush.org/code/ocaml-mysql/)
     (the version 1.0.4 is in the 'lib' directory)
 * MySQL server v.5.1 
     (http://dev.mysql.com/downloads/mysql/5.1.html#downloads)
 * make tools if you want to use make file.

*** You need to have access to some MySQL server and  ***
*** to set the correct connection informations in the ***
*** file 'access.ml' located in the same directory as ***
*** this REAME file.                                  ***

Given these, to compile the program, just issue

make

if successful, this will create an executable 'db' in ./bin 
folder. This executable can be consequently copied in any
convenient location.


II. Usage

DB is only able to load ontologies in OWL2 format.
For the usage options type:

./db --help     [on Linux & Mac]
  db --help     [on Windows]

For displaying progress bars, DB uses ANSI codes. 
Please use an ansi-capable terminal for optimal results.

Example:

./db -d galen -a galen.owl -x galen.tax

will load the ontology from file "galen.owl" and store it into
the database "galen", classifies it, and output the sorted 
taxonomy into the file "galen.tax"


III. Syntax

Currently DB can read ontologies in OWL 2 founctional-style
syntax and supports DL EL. 

For the description of OWL 2 functional-style syntax, see:
http://www.w3.org/TR/owl2-syntax/


IV. Conversion.

If you have an owl file [ontology.owl] in OWL or OWL 2 XML RDF syntax,
you can convert it into a functional syntax using Protege 4.
For this, the ontology [ontology.owl] should be loaded into Protege 4
and saved using menu File > Save as... > OWL Functional Syntax. 

Note that some versions of Protege 4 produce outdated OWL 2 syntax
and the result may reqire manual editing
(OWL 2 is still being standardazied and therefore subject to changes).
Alternatively, you could build the version of Protege 4 that uses
the most recent OWL api from the sources:
http://smi-protege.stanford.edu/repos/protege/protege4/protege-standalone/branches/protege4_1_port

