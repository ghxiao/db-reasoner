OCAMLMAKEFILE = ./OCamlMakefile

SOURCES =  access.ml
SOURCES += ./src/config.ml
SOURCES += ./src/config_load.ml
SOURCES += ./src/config_classify.ml
SOURCES += ./src/config_taxonomy.ml
SOURCES += ./src/config_output.ml
SOURCES += ./src/progressBar.mli ./src/progressBar.ml
SOURCES += ./src/dB_sig.mli      ./src/dB_sig.ml
SOURCES += ./src/dB_mysql.mli    ./src/dB_mysql.ml
SOURCES += ./src/common.mli      ./src/common.ml
SOURCES += ./src/buffers.mli     ./src/buffers.ml
SOURCES += ./src/dL.mli          ./src/dL.ml
SOURCES += ./src/fowl_parser.mly ./src/fowl_lexer.mll
SOURCES += ./src/output.mli      ./src/output.ml
SOURCES += ./src/taxonomy.mli     ./src/taxonomy.ml
SOURCES += ./src/classify.mli    ./src/classify.ml
SOURCES += ./src/load.mli        ./src/load.ml
SOURCES += ./src/main.ml

RESULT  = ./bin/db

INCDIRS = ../ 
INCDIRS += +mysql
LIBS = unix mysql

all: native-code

TRASH = *.o *.ml~ *.mli~ *.cmi *.cmx makefile~ ./src/*.o ./src/*.ml~ ./src/*.mli~ ./src/*.mll~ ./src/*.mly~ ./src/*.cmi ./src/*.cmx

include $(OCAMLMAKEFILE)
